// TEXT.HPP  Shabarshin A.A.  29.11.1996

/* THIS FILE IS PUBLIC DOMAIN */

#ifndef __STDIO_H
#include <stdio.h>
#endif
#ifndef __STDLIB_H
#include <stdlib.h>
#endif
#ifndef __STRING_H
#include <string.h>
#endif
#ifndef __TEXT_H
#define __TEXT_H

#ifndef UINT
#define UINT unsigned int
#endif

struct Line
{  Line *next;
   char *str;
   int type;
   int id;
   int id2;
   UINT adr;
   UINT len;
   Line(char *s);
  ~Line();
};

struct Text
{  Line *first;
   Line *last;
 private:
   long num;
   Line *find;
   char *find_str;
   int find_type;
   int find_id;
   int find_id2;
   UINT find_adr;
   int Replace(Line *l1);
 public:
   Text();
  ~Text(){AllDelete();};
   long GetNumber(void){return num;};
   int AllDelete(void);
   int Sort(int t=0); // 0-str, 1-type, 2-id, 3-id2, 4-adr
   int Delete(long n);
   Line* Insert(long n,char *s);
   Line* Add(char *s);
   Line* Get(long n);
   Line* FindFirst(char *s,int t=-1,int i=-1,int i2=-1,UINT a=0xFFFF);
   Line* FindNext(int m=0);
   long Index(Line *l);
   int Save(char *n);
   int Load(char *n);
   int List(void);
};

void TextError(int n)
{  char s[20];
   switch(n)
   { case 0: strcpy(s,"Out of Memory");break;
     case 1: strcpy(s,"Index Error");break;
     case 2: strcpy(s,"File Error");break;
     default:strcpy(s,"Unknown Error");
   }
   printf("\n\nTextError %u: %s !!!\n\n",n,s);
   exit(1);
};

char* GetWord(char *s,char *str,int n)
{
  int i=-1;
  int j=0;
  int m=-1;
  int spa=1;
  while(str[++i]!=0)
  {
     if(str[i]==' '||str[i]==9)
     {  spa=1;
        continue;
     }
     else
     {
        if(spa)
        {  m++;
           spa=0;
        }
        if(m==n) s[j++]=str[i];
     }
  }
  s[j]=0;
  return s;
}

//<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>

Line::Line(char *s)
{  str=new char[strlen(s)+1];
   if(str==NULL) TextError(0);
   strcpy(str,s);
   next=NULL;
   type=id=id2=0;
   adr=len=0;
}

Line::~Line()
{  if(str!=NULL) delete str;
}

Text::Text()
{  first=NULL;
   last=NULL;
   find_str=NULL;
   find=NULL;
   num=0L;
}

int Text::AllDelete(void)
{  int i;
   Line *l=first;
   Line *l2;
   while(l!=NULL)
   { l2=l->next;
     delete l;
     l=l2;
     num--;
   }
   first=NULL;
   last=NULL;
   find=NULL;
   return num;
}

int Text::Replace(Line *l1)
{  Line *l,*l2=l1->next;
   if(l1==first&&l2!=last)
   {  first=l2;
      l1->next=l2->next;
      l2->next=l1;
      return 2;
   }
   if(l1!=first&&l2==last)
   {  l=first;
      while(l->next!=l1) l=l->next;
      l->next=l2;
      l1->next=NULL;
      l2->next=l1;
      last=l1;
      return 2;
   }
   if(l1==first&&l2==last)
   {  first=l2;
      l1->next=NULL;
      l2->next=l1;
      last=l1;
      return 2;
   }
   l=first;
   while(l->next!=l1) l=l->next;
   l->next=l2;
   l1->next=l2->next;
   l2->next=l1;
   return 1;
}

int Text::Sort(int t)
{  int ff=1;
   Line *l1;
   Line *l2;
   if(num<1) return 0;
   switch(t)
   {  case 0: // str
              while(ff)
              {  ff=0;
                 l1=first;
                 while(l1->next!=NULL)
                 {  l2=l1->next;
                    if(strcmp(l1->str,l2->str)>0)
                    {  ff=1;
                       Replace(l1);
                    }
                    l1=l2;
                 }
              }
              break;
      case 1: // type
              while(ff)
              {  ff=0;
                 l1=first;
                 while(l1->next!=NULL)
                 {  l2=l1->next;
                    if(l1->type > l2->type)
                    {  ff=1;
                       Replace(l1);
                    }
                    l1=l2;
                 }
              }
              break;
      case 2: // id
              while(ff)
              {  ff=0;
                 l1=first;
                 while(l1->next!=NULL)
                 {  l2=l1->next;
                    if(l1->id > l2->id)
                    {  ff=1;
                       Replace(l1);
                    }
                    l1=l2;
                 }
              }
              break;
      case 3: // id2
              while(ff)
              {  ff=0;
                 l1=first;
                 while(l1->next!=NULL)
                 {  l2=l1->next;
                    if(l1->id2 > l2->id2)
                    {  ff=1;
                       Replace(l1);
                    }
                    l1=l2;
                 }
              }
              break;
      case 4: // adr
              while(ff)
              {  ff=0;
                 l1=first;
                 while(l1->next!=NULL)
                 {  l2=l1->next;
                    if(l1->adr > l2->adr)
                    {  ff=1;
                       Replace(l1);
                    }
                    l1=l2;
                 }
              }
              break;
      default: TextError(3);
   }
   return 1;
}

Line* Text::Insert(long n,char *s)
{  if(n>num) TextError(1);
   Line *l,*lp;
   if(n==num) l=Add(s);
   else
   {  if(n==0L)
      {  l=first;
         first=new Line(s);
         if(first==NULL) TextError(0);
         first->next=l;
         l=first;
      }
      else
      {  lp=first;
         for(long i=0L;i<n-1;i++) lp=lp->next;
         l=lp->next;
         lp->next=new Line(s);
         if(lp->next==NULL) TextError(0);
         lp->next->next=l;
         l=lp->next;
      }
      num++;
   }
   return l;
}

Line* Text::Add(char *s)
{  Line *l=new Line(s);
   if(l==NULL) TextError(0);
   if(num) last->next=l;
   else first=l;
   last=l;
   num++;
   return l;
}

int Text::Delete(long n)
{  Line *l,*lp;
   if(n>=num) TextError(1);
   if(n)
   {  lp=first;
      for(long i=0L;i<n-1;i++) lp=lp->next;
      l=lp->next;
      if(n==num-1) last=lp;
      lp->next=l->next;
   }
   else
   {  l=first;
      first=l->next;
   }
   delete l;
   num--;
   return 1;
}

Line* Text::Get(long n)
{  if(n>=num) TextError(1);
   Line *l=first;
   for(long i=0L;i<n;i++) l=l->next;
   return l;
}

Line* Text::FindFirst(char *s,int t,int i,int i2,UINT a)
{  Line *l;
   find_str=s;
   find_type=t;
   find_id=i;
   find_id2=i2;
   find_adr=a;
   find=NULL;
   return FindNext(1);
}

Line* Text::FindNext(int m)
{  Line *l;
   Line l0("#");
   l0.next = first;
   if(m) l = &l0;
   else l = find;
   int f=1;
   if(l!=NULL)
   {  while(f)
      {
         l=l->next;
         if(l==NULL) break;
         f=5;
         if(find_str==NULL) f--;
         else{if(!strcmp(find_str,l->str)) f--;}
         if(find_type==-1) f--;
         else{if(find_type==l->type) f--;}
         if(find_id==-1) f--;
         else{if(find_id==l->id) f--;}
         if(find_id2==-1) f--;
         else{if(find_id2==l->id2) f--;}
         if(find_adr==0xFFFF) f--;
         else{if(find_adr==l->adr) f--;}
      }
   }
   find = l;
   return l;
}

long Text::Index(Line *l)
{  long i=0;
   Line *l2=first;
   while(l2!=l)
   {  i++;
      l2=l2->next;
   }
   return i;
}

int Text::Save(char *n)
{  FILE *f=fopen(n,"wt");
   if(f==NULL) TextError(2);
   Line *l=first;
   while(l!=NULL)
   {  fputs(l->str,f);
      fputc('\n',f);
      l=l->next;
   }
   fclose(f);
   return 1;
}

int Text::Load(char *n)
{  FILE *f=fopen(n,"rt");
   if(f==NULL) TextError(2);
   char *buf=new char[256];
   char *po;
   if(buf==NULL) TextError(0);
   AllDelete();
   num=0L;
   int i;
   char c=0;
   while(!feof(f))
   {  i=0;c=0;
      while(!feof(f)&&c!='\n'&&i<255)
      {  c=fgetc(f);
         if(c=='\r'||c=='\n'||feof(f)) buf[i++]=0;
         else buf[i++]=c;
      }
      if(i==255) buf[i]=0;
      Add(buf);
   }
   delete buf;
   fclose(f);
   return 1;
}

int Text::List(void)
{  Line *l;
   for(l=first;l!=NULL;l=l->next)
       printf("\n%i\t%i\t%i\t%4.4X\t%u\t%s",
              l->type,l->id,l->id2,l->adr,l->len,l->str);
   return 1;
}


#endif
