// MY_VGA.HPP       Shabarshin A.A
// 320x200 256 colors.  12.03.1997
// Only for Borland-C++ !!!

#ifndef __STDIO_H
#include <stdio.h>
#endif
#ifndef __STDLIB_H
#include <stdlib.h>
#endif
#ifndef __STRING_H
#include <string.h>
#endif
#ifndef __DOS_H
#include <dos.h>
#endif
#ifndef _NO_FONT
#include "font5x9.h"
#endif
#ifndef __MY_VGA_H
#define __MY_VGA_H

#define MAX_CVGA 63
#ifndef BYTE
#define BYTE unsigned char
#endif
#define VADR(x,y) ((x)+s320[(y)])

#define DX 320
#define DY 200

int dX = 320;
int dY = 200;

int fVGA = 0;
long s320[200];
BYTE far *VideoMem;

#ifndef _RGB_STRUCT
#define _RGB_STRUCT
struct RGB
{  BYTE r,g,b;
   RGB(int r1,int g1,int b1){r=r1;g=g1;b=b1;};
   RGB(){r=MAX_CVGA;g=MAX_CVGA;b=MAX_CVGA;};
};
#endif

//   of the value read changes from 1 to 0, meaning a vertical
//   retrace just started.
void TraceWait(void)
{
  retracewait1:
     asm {
          mov  dx,0x3DA
          in   al,dx
          and  al,8
          jnz  retracewait1
     }
  retracewait2:
     asm {
          mov  dx,0x3DA
          in   al,dx
          and  al,8
          jz   retracewait2
     }
}


#ifndef _NO_FONT

short DrawChar(short x0,short y0,short ch,short cs,short cb)
{
    if(ch<32||ch>127) return 0;
    short i,j,k,r;
    unsigned char *vptr0 = VideoMem + VADR(x0,y0);
    unsigned char *vptr;
    for(j=0;j<9;j++)
    {  r = font5x9[ch-32][j];
       vptr = vptr0 + s320[j];
       *vptr++ = cb;
       for(i=0;i<5;i++)
       {  if(r&1) *vptr=cs;
          else *vptr=cb;
          r>>=1;
          vptr++;
       }
       *vptr++ = cb;
       *vptr++ = cb;
    }
    return 1;
}

short DrawString(short x0,short y0,char *s,short cs,short cb)
{
    for(int i=0;i<strlen(s);i++)
    {
       int x = x0+i*8;
       if(x>=320) break;
       DrawChar(x,y0,s[i],cs,cb);
    }
    return 1;
}

#endif

void SetVideoMode(int mode)
{
   asm {
       mov ax,mode
       int 10h
   }
}

int InitGraph(void)
{   if(fVGA) return 0;
    fVGA=1;
    VideoMem=(char*)MK_FP(0xA000,0);
    int j=0;
    for(long i=0;i<64000L;i+=320) s320[j++]=i;
    SetVideoMode(0x13);
    return 1;
}

inline void CloseGraph(void)
{
    SetVideoMode(0x03);
}

inline void PutScreenPixel(int x,int y,int c)
{
    VideoMem[x+s320[y]]=c;
}

inline int GetScreenPixel(int x,int y)
{
    return VideoMem[x+s320[y]];
}

int DrawBar(int x0,int y0,int dx,int dy,int c)
{
    int i,j;
    if(x0<0)
    {  dx=dx+x0;
       x0=0;
    }
    if(y0<0)
    {  dy=dy+y0;
       y0=0;
    }
    if((x0+dx)>=dX) dx=dX-x0;
    if((y0+dy)>=dY) dy=dY-y0;
    unsigned int ab=s320[y0]+x0,at;
    for(j=0;j<dy;j++,ab+=320){ at=ab;
    for(i=0;i<dx;i++,at++){
        VideoMem[at] = c;
    }}
    return c;
}

inline int Cls(int c=0)
{
    DrawBar(0,0,dX-1,dY-1,c);
    return c;
}

int HLine(int x0,int y0,int dx,int c)
{
    if(x0<0)
    {  dx=dx+x0;
       x0=0;
    }
    int i;
    long a = s320[y0] + x0;
    for(i=x0;i<x0+dx;i++,a++)
    {   if(i>=dX) break;
        VideoMem[a] = c;
    }
    return c;
}

int VLine(int x0,int y0,int dy,int c)
{
    if(y0<0)
    {  dy=dy+y0;
       y0=0;
    }
    int j;
    long a = s320[y0] + x0;
    for(j=y0;j<y0+dy;j++,a+=dX)
    {   if(j>=dY) break;
        VideoMem[a] = c;
    }
    return c;
}

struct Palette256
{
     BYTE d[256][3];
     Palette256(){InitGraph();};
     Palette256(char *p)
     {  InitGraph();
        LoadPalette(p);
        SetHardPalette();
     };
     int LoadPalette(char* s);
     int SavePalette(char* s);
     ~Palette256(){;};
     void SetPalette(int i,RGB col)
     {  d[i][0]=col.r;
        d[i][1]=col.g;
        d[i][2]=col.b;
     };
     void SetHardPalette(void)
     {  struct REGPACK reg;
        reg.r_ax = 0x1012;
        reg.r_bx = 0;
        reg.r_cx = 256;
        reg.r_es = FP_SEG(d);
        reg.r_dx = FP_OFF(d);
        intr(0x10,&reg);
     };
     RGB GetPalette(int i)
     {  RGB col;
        col.r=d[i][0];
        col.g=d[i][1];
        col.b=d[i][2];
        return col;
     };
     void GetHardPalette(void)
     {  struct REGPACK reg;
        reg.r_ax = 0x1017;
        reg.r_bx = 0;
        reg.r_cx = 256;
        reg.r_es = FP_SEG(d);
        reg.r_dx = FP_OFF(d);
        intr(0x10,&reg);
     };
};

int Palette256::LoadPalette(char* s)
{  int b,g,r,i;
   char ss[100];
   strcpy(ss,s);
   strcat(ss,".pal");
   FILE *f=fopen(ss,"rb");
   for(i=0;i<256;i++)
   {
       r=fgetc(f);
       g=fgetc(f);
       b=fgetc(f);
       SetPalette(i,RGB(r,g,b));
   }
   fclose(f);
   return 1;
}

int Palette256::SavePalette(char* s)
{  int i;
   RGB p;
   char ss[100];
   strcpy(ss,s);
   strcat(ss,".pal");
   FILE *f=fopen(ss,"wb");
   for(i=0;i<256;i++)
   {
       p=GetPalette(i);
       fputc(p.r,f);
       fputc(p.g,f);
       fputc(p.b,f);
   }
   fclose(f);
   return 1;
}

#endif
