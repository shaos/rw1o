/*
 Robot Warfare 1 Open Game
 =========================

 Copyright (c) 1998-2013 A.A.Shabarshin <me@shaos.net>

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom
 the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

// ROBOTWAR.CPP - Shabarshin A.A.  10.11.1998

#include "robotwar.h"
#include "text.hpp"
#include "wildcmp.hpp"

//#define RDUMP

char* Robot::SkipSpace(char* p)
{
  while(*p==' '||*p=='\n') p++;
  return p;
}

void Robot::StrUp(char *str)
{
  int f=1;
  for(int i=0;i<strlen(str);i++)
  {
     if(str[i]=='"')
     {
        if(f) f=0;
        else  f=1;
     }
     if(f) str[i]=toupper(str[i]);
  }
}

int Robot::StartWith(char *str,char *sw,short f)
{
   char *p1,*p2;
   p1 = sw;
   p2 = str;
   while(*p1!=0)
   {
     if(*p2==0) return 0;
     if(*p1!=*p2) return 0;
     p1++;
     p2++;
   }
   if(f&&(*p2!=' '&&*p2!='\t'&&*p2!='\n'&&*p2!=0)) return 0;
   return 1;
}

void Robot::Error(int e,char *s,short i)
{
  char str[100];
  switch (e)
  {
    case 0 : strcpy(str,"Out of memory");break;
    case 1 : strcpy(str,"Can't open file");break;
    case 2 : strcpy(str,"Illegal extention");break;
    case 3 : strcpy(str,"Too big name");break;
    case 4 : strcpy(str,"Syntax error");break;
    case 5 : strcpy(str,"Quotation is expected");break;
    case 6 : strcpy(str,"Duplicate definition");break;
    case 7 : strcpy(str,"Definition after START:");break;
    case 8 : strcpy(str,"Command before START:");break;
    case 9 : strcpy(str,"Too many vars");break;
    case 10: strcpy(str,"Unknown name");break;
    case 11: strcpy(str,"Out of index");break;
    case 12: strcpy(str,"Register is read only");break;
    case 13: strcpy(str,"Command after END");break;
    case 14: strcpy(str,"END is expected");break;
    case 15: strcpy(str,"Bad file format");break;
    case 16: strcpy(str,"Too many lines");break;
    case 17: strcpy(str,"Unknown label");break;
    default: strcpy(str,"Unknown error");break;
  }
  printf("\n");
  if(i>0) printf("(%u) ",i);
  printf("ERROR %i: %s !",e,str);
  if(s!=NULL) printf("\n\t%s",s);
  printf("\n\n");
  exit(1);
}

short Robot::Hex(char ch)
{
  short i=0;
  switch(ch)
  { case '0': i=0; break;
    case '1': i=1; break;
    case '2': i=2; break;
    case '3': i=3; break;
    case '4': i=4; break;
    case '5': i=5; break;
    case '6': i=6; break;
    case '7': i=7; break;
    case '8': i=8; break;
    case '9': i=9; break;
    case 'A': i=10; break;
    case 'B': i=11; break;
    case 'C': i=12; break;
    case 'D': i=13; break;
    case 'E': i=14; break;
    case 'F': i=15; break;
    case 'a': i=10; break;
    case 'b': i=11; break;
    case 'c': i=12; break;
    case 'd': i=13; break;
    case 'e': i=14; break;
    case 'f': i=15; break;
  }
  return i;
}

Robot::Robot(short num)
{
   number = num;
   Init();
}

Robot::Robot(short num,char *s)
{
   int i,j,k;
   number = num;
   Init();
   char str[100];
   strcpy(str,s);
   StrUp(str);
   int len=strlen(str);
   if(len<4)Error(2,s);
   if(str[len-4]!='.'||str[len-3]!='R'||str[len-2]!='W')Error(2,s);
   if(str[len-1]=='0')
   {
     FILE *f=fopen(s,"rb");
     fseek(f,0L,SEEK_END);
     long fsize = ftell(f);
     fseek(f,0L,SEEK_SET);
     if(fgetc(f)>1) Error(15,s); // VER 1.99
     unsigned short ks=0,ksf,offs;
     ksf = fgetc(f);
     ksf += (unsigned short)fgetc(f)<<8;
     offs = fgetc(f);
     offs += (unsigned short)fgetc(f)<<8;
     for(i=5;i<fsize;i++) ks+=fgetc(f);
//     printf("KC=%u(%4.4X) KCF=%u(%4.4X)\n",ks,ks,ksf,ksf);
     if(ks!=ksf) Error(15,s);
     fseek(f,5L,SEEK_SET);
     j = fgetc(f);
     if(j>=32) Error(3,"ROBOTNAME");
     for(i=0;i<j;i++) name[i]=fgetc(f);
     name[i] = 0;
     if(fgetc(f)!=0) Error(15,s);
     nvar = fgetc(f);
     nvar += (unsigned short)fgetc(f)*256U;
     cur = fgetc(f);
     cur += (unsigned short)fgetc(f)*256U;
     color = fgetc(f);
     color<<=8;
     color |= fgetc(f);
     color<<=8;
     color |= fgetc(f);
     for(i=0;i<4;i++) equip[i]=fgetc(f);
     j = fgetc(f);
     fimage = 0;
     if(j!=0&&j!=0x20) Error(15,s);
     if(j==0x20)
     {
        for(j=0;j<8;j++){
        for(i=0;i<8;i+=2){
           k = fgetc(f);
           image[i][j] = (k&0xF0)>>4;
           image[i+1][j] = k&0x0F;
        }}
        fimage = 1;
     }
     i = 0;
     while(1)
     {
        if(ftell(f)>=offs) break;
        author[i] = fgetc(f);
        if(author[i]==0) break;
        if(++i>=32) Error(3,"AUTHORNAME");
     }
     author[i]=0;
     fseek(f,offs,SEEK_SET);
     code = new unsigned char[cur];
     if(code==NULL) Error(0,"code");
     for(i=0;i<cur;i++) code[i]=fgetc(f);
     if(code[cur-1]!=0xFF) Error(15,s);
     fclose(f);
     printf("\n name=%s\n",name);
     printf(" var=%u\n",nvar);
     printf(" code=%u\n",cur);
   }
   if(str[len-1]=='1')
   { Line *l;
     Text t;
     t.Load(s);
     i=0;
     for(l=t.first;l!=NULL;l=l->next)
     {  i++;
        l->id2=0;
        l->id=i;
     }
     // Preprocessing
     if(Prepro(&t))
     {  str[strlen(str)-1]='_';
        t.Save(str);
     }
     Compile(&t);
   }
}

Robot::~Robot()
{
   delete str;
   delete var;
}

#define HEX2(x1,x2) (((x1)<<4)|(x2))

#define MAXTRI 2000

struct Triplet
{
  unsigned char b1[5]; // Variable 1
  unsigned char b2[5]; // Variable 2
  unsigned char b3[5]; // Variable 3
  unsigned short adr;
  unsigned short w;
  char *str;
  short len;
};

void Robot::Compile(Text *t)
{
   Line *l,*l2,*l3;
   char str[100];
   char st2[100];
   char st3[100];
   short files[MAXLINE];
   short lines[MAXLINE];
   char b5[5];
   char *p0,*p1,*p2,*p3,*p4,*p5;
   short *psh;
   int i,j,k;
   i=0;k=0;
   for(l=t->first;l!=NULL;l=l->next)
   {  i++;
      files[k]=l->id2;
      lines[k++]=l->id;
      if(k>MAXLINE) Error(16);
      if(l->str[0]==0) continue;
      p1=l->str;
      p2=l->str;
      while(*p2==' '||*p2=='\t') p2++;
      while(*p2!=0)
      { *p1=*p2;
        p1++;p2++;
      }
      *p1=0;
      if(l->str[0]=='%') continue;
      j = strlen(l->str)-1;
      while(l->str[j]==' '||l->str[j]=='\t')
      {  l->str[j] = 0;
         j--;
      }
      if(l->str[0]==0) continue;
      StrUp(l->str);
   }
   cur=0;
   nvar=0;
   Text vars;
   l=vars.Add("X");l->type=1;l->adr=0xFF00U;
   l=vars.Add("Y");l->type=1;l->adr=0xFF01U;
   l=vars.Add("D");l->type=1;l->adr=0xFF02U;
   l=vars.Add("N");l->type=1;l->adr=0xFF03U;
   l=vars.Add("K");l->type=1;l->adr=0xFF04U;
   l=vars.Add("R");l->type=1;l->adr=0xFF05U;
   l=vars.Add("T");l->type=1;l->adr=0xFF06U;
   l=vars.Add("E");l->type=1;l->adr=0xFF07U;
   l=vars.Add("M");l->type=1;l->adr=0xFF08U;
   l=vars.Add("I");l->type=1;l->adr=0xFF09U;
   l=vars.Add("S");l->type=1;l->adr=0xFF0FU;
   i=0;
   Triplet *ttt = new Triplet[MAXTRI];
   if(ttt==NULL) Error(0,"Triplet");
   for(i=0;i<MAXTRI;i++) ttt[i].str=NULL;
   int frobot  = 0;
   int fauthor = 0;
   int fcolor  = 0;
   int fbody   = 0;
   int nimage  = 0;
   int ntripl  = 0;
   int ffront  = 0;
   int fback   = 0;
   int fleft   = 0;
   int fright  = 0;
   int fend    = 0;
   int ln = -1;
   for(l=t->first;l!=NULL;l=l->next)
   {  ln++;
      if(l->str[0]==0){l->id=0;continue;}
      if(l->str[0]=='%'){l->id=0;continue;}
      if(fend) Error(13,l->str,lines[ln]);
      strcpy(str,l->str);
      p1=str;
      int ok=0;
      if(StartWith(l->str,"ROBOT",1))
      {
         if(fbody) Error(7,l->str,lines[ln]);
         if(frobot) Error(6,l->str,lines[ln]);
         l->id=0;
         frobot=1;
         p1=SkipSpace(p1+5);
         if(*p1!='"') Error(5,l->str,lines[ln]);
         p1++;
         for(j=0;j<16;j++)
         {
            if(*p1==0) Error(5,l->str,lines[ln]);
            if(*p1=='"') break;
            name[j]=*p1;
            p1++;
         }
         name[j]=0;
         while(*p1!='"'&&*p1!=0) p1++;
         if(*p1==0) Error(5,l->str,lines[ln]);
         p1=SkipSpace(p1+1);
         if(*p1!='%'&&*p1!=0) Error(4,l->str,lines[ln]);
         ok=1;
         continue;
      }
      if(StartWith(l->str,"AUTHOR",1))
      {
         if(fbody) Error(7,l->str,lines[ln]);
         if(fauthor) Error(6,l->str,lines[ln]);
         l->id=0;
         fauthor=1;
         p1=SkipSpace(p1+6);
         if(*p1!='"') Error(5,l->str,lines[ln]);
         p1++;
         for(j=0;j<16;j++)
         {
            if(*p1==0) Error(5,l->str,lines[ln]);
            if(*p1=='"') break;
            author[j]=*p1;
            p1++;
         }
         author[j]=0;
         while(*p1!='"'&&*p1!=0) p1++;
         if(*p1==0) Error(5,l->str,lines[ln]);
         p1=SkipSpace(p1+1);
         if(*p1!='%'&&*p1!=0) Error(4,l->str,lines[ln]);
         ok=1;
         continue;
      }
      if(StartWith(l->str,"COLOR",1))
      {
         if(fbody) Error(7,l->str,lines[ln]);
         if(fcolor) Error(6,l->str,lines[ln]);
         l->id=0;
         fcolor=1;
         p1=SkipSpace(p1+5);
         p2 = strchr(p1,'%');
         if(p2!=NULL) *p2=0;
         else p2 = &p1[strlen(p1)];
         p2--;
         while(*p2==' '||*p2=='\t'){*p2=0;p2--;};
         if(strlen(p1)!=6) Error(4,l->str,lines[ln]);
         long rr = HEX2(Hex(p1[0]),Hex(p1[1]));
         long gg = HEX2(Hex(p1[2]),Hex(p1[3]));
         long bb = HEX2(Hex(p1[4]),Hex(p1[5]));
         color = (rr<<16)|(gg<<8)|bb;
         ok=1;
         continue;
      }
      if(StartWith(l->str,"IMAGE",1))
      {
         if(fbody) Error(7,l->str,lines[ln]);
         if(nimage>=8) Error(6,l->str,lines[ln]);
         l->id=0;
         p1=SkipSpace(p1+5);
         for(j=0;j<8;j++)
         {
           if(*p1==0||*p1=='%') Error(4,l->str,lines[ln]);
           image[j][nimage] = Hex(*p1);
           p1 = SkipSpace(p1+1);
         }
         if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
         nimage++;
         fimage=1;
         ok=1;
         continue;
      }
      if(StartWith(l->str,"FRONT",1))
      {
         if(fbody) Error(7,l->str,lines[ln]);
         if(ffront) Error(6,l->str,lines[ln]);
         l->id=0;
         p1=SkipSpace(p1+5);
         j=0;
         if(StartWith(p1,"EYE",1)){j=1;equip[0]=1;}
         if(StartWith(p1,"GUN",1)){j=1;equip[0]=2;}
         if(j==0) Error(4,l->str,lines[ln]);
         p1=SkipSpace(p1+3);
         if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
         ffront=1;
         ok=1;
         continue;
      }
      if(StartWith(l->str,"BACK",1))
      {
         if(fbody) Error(7,l->str,lines[ln]);
         if(fback) Error(6,l->str,lines[ln]);
         l->id=0;
         p1=SkipSpace(p1+4);
         j=0;
         if(StartWith(p1,"EYE",1)){j=1;equip[2]=1;}
         if(StartWith(p1,"GUN",1)){j=1;equip[2]=2;}
         if(j==0) Error(4,l->str,lines[ln]);
         p1=SkipSpace(p1+3);
         if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
         fback=1;
         ok=1;
         continue;
      }
      if(StartWith(l->str,"LEFT",1))
      {
         p1=SkipSpace(p1+4);
         if(fbody)
         {
           if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
           l->adr = cur++;
           l->id = 0x35;
         }
         else
         {
           if(fleft) Error(6,l->str,lines[ln]);
           l->id=0;
           j=0;
           if(StartWith(p1,"EYE",1)){j=1;equip[3]=1;}
           if(StartWith(p1,"GUN",1)){j=1;equip[3]=2;}
           if(j==0) Error(4,l->str,lines[ln]);
           p1=SkipSpace(p1+3);
           if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
           fleft=1;
         }
         ok=1;
         continue;
      }
      if(StartWith(l->str,"RIGHT",1))
      {
         p1=SkipSpace(p1+5);
         if(fbody)
         {
           if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
           l->adr = cur++;
           l->id = 0x36;
         }
         else
         {
           if(fright) Error(6,l->str,lines[ln]);
           l->id=0;
           j=0;
           if(StartWith(p1,"EYE",1)){j=1;equip[1]=1;}
           if(StartWith(p1,"GUN",1)){j=1;equip[1]=2;}
           if(j==0) Error(4,l->str,lines[ln]);
           p1=SkipSpace(p1+3);
           if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
           fright=1;
         }
         ok=1;
         continue;
      }
      if(StartWith(l->str,"GOTO",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+4);
         p2=strtok(p1," \t");
         l2 = vars.FindFirst(p2,0);
         if(l2==NULL)
         {
            l2 = vars.Add(p2);
            l2->type = -1;
            l2->adr = t->Index(l);
         }
         l->adr = cur;
         l->id = 0x30;
         l->id2 = l2->adr;
         cur += 3;
         p1=strtok(NULL," \t");
         if(p1!=NULL&&*p1!='%') Error(4,l->str,lines[ln]);
         ok=1;
         continue;
      }
      if(StartWith(l->str,"CALL",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+4);
         p2=strtok(p1," \t");
         l2 = vars.FindFirst(p2,0);
         if(l2==NULL)
         {
            l2 = vars.Add(p2);
            l2->type = -1;
            l2->adr = t->Index(l);
         }
         l->adr = cur;
         l->id = 0x31;
         l->id2 = l2->adr;
         cur += 3;
         p1=strtok(NULL," \t");
         if(p1!=NULL&&*p1!='%') Error(4,l->str,lines[ln]);
         ok=1;
         continue;
      }
      if(StartWith(l->str,"RET",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+3);
         if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
         l->adr = cur++;
         l->id = 0x33;
         ok=1;
         continue;
      }
      if(StartWith(l->str,"STEP",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+4);
         if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
         l->adr = cur++;
         l->id = 0x34;
         ok=1;
         continue;
      }
      if(StartWith(l->str,"SAY",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+3);
         if(*p1!='"') Error(5,l->str,lines[ln]);
         p1++;
         for(j=0;j<100;j++)
         {
            if(*p1=='"') break;
            if(*p1==0) break;
            st2[j]=*p1;
            p1++;
         }
         st2[j]=0;
         ttt[ntripl].b1[0]=99;
         ttt[ntripl].str = new char[strlen(st2)+1];
         if(ttt[ntripl].str==NULL) Error(0,"Compile",lines[ln]);
         strcpy(ttt[ntripl].str,st2);
         l->adr = cur;
         l->id = 0x37;
         l->id2 = ntripl;
         cur += strlen(st2)+2;
         ntripl++;
         if(ntripl>=MAXTRI) Error(99,l->str,lines[ln]);
         if(*p1!='"') Error(4,l->str,lines[ln]);
         p1=SkipSpace(p1+1);
         if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
         ok=1;
         continue;
      }
      if(StartWith(l->str,"PRINT",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+5);
         p2=strtok(p1," \t");
         CompName(&vars,p2,(char*)b5,l->str,0,lines[ln]);
         l->adr = cur;
         l->id = 0x38;
         l->type = b5[0];
         l->len = b5[1]+(b5[2]<<8);
         l->id2 = b5[3]+(b5[4]<<8);
         if(b5[0]<2) cur+=4;
         else cur+=6;
         p1=strtok(NULL," \t");
         if(p1!=NULL&&*p1!='%') Error(4,l->str,lines[ln]);
         ok=1;
         continue;
      }
      if(StartWith(l->str,"TEST",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+4);
         if(*p1!='"') Error(5,l->str,lines[ln]);
         p1++;
         for(j=0;j<100;j++)
         {
            if(*p1=='"') break;
            if(*p1==0) break;
            st2[j]=*p1;
            p1++;
         }
         st2[j]=0;
         ttt[ntripl].b1[0]=100;
         ttt[ntripl].str = new char[strlen(st2)+1];
         if(ttt[ntripl].str==NULL) Error(0,"Compile",lines[ln]);
         strcpy(ttt[ntripl].str,st2);
         l->adr = cur;
         l->id = 0x51;
         l->id2 = ntripl;
         cur += strlen(st2)+4;
         if(*p1!='"') Error(4,l->str,lines[ln]);
         p1=SkipSpace(++p1);
         if(*p1!=':') Error(4,l->str,lines[ln]);
         p1=SkipSpace(++p1);
         p2=strtok(p1," \t");
         l2 = vars.FindFirst(p2,0);
         if(l2==NULL)
         {
            l2 = vars.Add(p2);
            l2->type = -1;
            l2->adr = t->Index(l);
         }
         ttt[ntripl].b3[0] = 98;
         ttt[ntripl].b3[1] = (l2->adr)&0xFF;
         ttt[ntripl].b3[2] = (l2->adr)>>8;
         p2=strtok(NULL," \t");
         ntripl++;
         if(ntripl>=MAXTRI) Error(99,l->str,lines[ln]);
         if(p2!=NULL&&*p2!='%') Error(4,l->str,lines[ln]);
         ok=1;
         continue;
      }
      if(StartWith(l->str,"DUMP",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+4);
         p2=strtok(p1," \t");
         l->adr = cur;
         l->id = 0x3F;
         l->id2 = 0x00;
         l->len = atoi(p2);
         cur+=3;
         p1=strtok(NULL," \t");
         if(p1!=NULL&&*p1!='%') Error(4,l->str,lines[ln]);
         ok=1;
         continue;
      }
      if(StartWith(l->str,"DEF",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         l->id=0;
         p1=SkipSpace(p1+3);
         strcpy(st2,p1);
         p2 = strchr(st2,'[');
         if(p2!=NULL) *p2=0;
         else Error(4,l->str,lines[ln]);
         p2++;
         p3 = strchr(p2,']');
         if(p3!=NULL) *p3=0;
         else Error(4,l->str,lines[ln]);
         p3++;
         p3=SkipSpace(p3);
         k = 0x01;
         if(*p3!=0&&*p3!='%')
         {
            if(*p3!='=') Error(4,l->str,lines[ln]);
            p3=SkipSpace(p3+1);
            if(*p3!='{') Error(4,l->str,lines[ln]);
            p3++;
            p4 = strchr(p3,'}');
            if(p4!=NULL) *p4=0;
            else Error(4,l->str,lines[ln]);
            p4++;
            p4=SkipSpace(p4);
            if(*p4!=0&&*p4!='%') Error(4,l->str,lines[ln]);
            i=1;
            p5=p4=p3;
            while(*p4!=0)
            {
               if(*p4==',') i++;
               if(*p4==' '||*p4=='\t') p4++;
               *p5=*p4;
               p4++;p5++;
            }
            *p5=0;
            ttt[ntripl].b1[0] = nvar%256;
            ttt[ntripl].b1[1] = nvar/256;
            ttt[ntripl].b1[2] = i%256;
            ttt[ntripl].b1[3] = i/256;
            ttt[ntripl].str = new char[2*i];
            ttt[ntripl].len = 2*i;
            psh = (short*)ttt[ntripl].str;
            ntripl++;
            p4 = strtok(p3,",");
            for(j=0;j<i;j++)
            {
               psh[j] = atoi(p4);
               p4 = strtok(NULL,",");
            }
            k = 0x02;
         }
         l2 = vars.FindFirst(str);
         if(l2!=NULL) Error(6,l->str,lines[ln]);
         j = atoi(p2);
         if(j<1) Error(4,l->str,lines[ln]);
         if(j>=VARMAX) Error(9,l->str,lines[ln]);
         if((long)nvar+j>=(long)VARMAX) Error(4,l->str,lines[ln]);
         l2 = vars.Add(st2);
         l2->type = 2;
         l2->adr = nvar;
         l2->len = j;
         l->id = k;
         if(k==1) l->id2 = nvar;
         else l->id2 = ntripl-1;
         l->len = j;
         l->adr = cur;
         if(k==1) cur += 5;
         else cur += 7+2*i;
         nvar += j;
         ok=1;
         continue;
      }
      if(StartWith(l->str,"ACT",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+3);
         p2=strtok(p1," \t");
         k=0;b5[0]=b5[2]=b5[3]=b5[4]=0;
         if(!strcmp(p2,"FRONT")){k=1;b5[1]=0;}
         if(!strcmp(p2,"RIGHT")){k=1;b5[1]=1;}
         if(!strcmp(p2,"BACK")){k=1;b5[1]=2;}
         if(!strcmp(p2,"LEFT")){k=1;b5[1]=3;}
         if(!k) CompName(&vars,p2,(char*)b5,l->str,0,lines[ln]);
         l->adr = cur;
         l->id = 0x3A;
         l->type = b5[0];
         l->len = b5[1]+(b5[2]<<8);
         l->id2 = b5[3]+(b5[4]<<8);
         if(b5[0]<2) cur+=4;
         else cur+=6;
         p1=strtok(NULL," \t");
         if(p1!=NULL&&*p1!='%') Error(4,l->str,lines[ln]);
         ok=1;
         continue;
      }
      if(StartWith(l->str,"SPY",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+3);
         if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
         l->adr = cur++;
         l->id = 0x3B;
         ok=1;
         continue;
      }
      if(StartWith(l->str,"RADAR",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+5);
         p2=strtok(p1," \t");
         CompName(&vars,p2,(char*)b5,l->str,0,lines[ln]);
         l->adr = cur;
         l->id = 0x3C;
         l->type = b5[0];
         l->len = b5[1]+(b5[2]<<8);
         l->id2 = b5[3]+(b5[4]<<8);
         if(b5[0]<2) cur+=4;
         else cur+=6;
         p1=strtok(NULL," \t");
         if(p1!=NULL&&*p1!='%') Error(4,l->str,lines[ln]);
         ok=1;
         continue;
      }
      if(StartWith(l->str,"SHOT",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+4);
         p2=strtok(p1," \t");
         CompName(&vars,p2,(char*)b5,l->str,0,lines[ln]);
         l->adr = cur;
         l->id = 0x60;
         l->type = b5[0];
         l->len = b5[1]+(b5[2]<<8);
         l->id2 = b5[3]+(b5[4]<<8);
         if(b5[0]<2) cur+=4;
         else cur+=6;
         p1=strtok(NULL," \t");
         if(p1!=NULL&&*p1!='%') Error(4,l->str,lines[ln]);
         ok=1;
         continue;
      }
      if(StartWith(l->str,"SEND",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p0=SkipSpace(p1+4);
         p1=strtok(p0," \t");
         p2=strtok(NULL," \t");
         CompName(&vars,p1,(char*)ttt[ntripl].b1,l->str,0,lines[ln]);
         if(p2==NULL) ttt[ntripl].b2[0]=ttt[ntripl].b2[1]=ttt[ntripl].b2[2]=0;
         else CompName(&vars,p2,(char*)ttt[ntripl].b2,l->str,0,lines[ln]);
         j = 1;
         if(ttt[ntripl].b1[0]<2) j+=3;
         else j+=5;
         if(ttt[ntripl].b2[0]<2) j+=3;
         else j+=5;
         l->adr = cur;
         l->id = 0x61;
         l->type = 101;
         l->id2 = ntripl;
         cur += j;
         ntripl++;
         if(ntripl>=MAXTRI) Error(99,l->str,lines[ln]);
         p1=strtok(NULL," \t");
         if(p1!=NULL&&*p1!='%') Error(4,l->str,lines[ln]);
         ok=1;
         continue;
      }
      if(StartWith(l->str,"RECV",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+4);
         p2=strtok(p1," \t");
         CompName(&vars,p2,(char*)b5,l->str,1,lines[ln]);
         l->adr = cur;
         l->id = 0x62;
         l->type = b5[0];
         l->len = b5[1]+(b5[2]<<8);
         l->id2 = b5[3]+(b5[4]<<8);
         if(b5[0]<2) cur+=4;
         else cur+=6;
         p1=strtok(NULL," \t");
         if(p1!=NULL&&*p1!='%') Error(4,l->str,lines[ln]);
         ok=1;
         continue;
      }
      if(StartWith(l->str,"POP",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+3);
         if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
         l->adr = cur++;
         l->id = 0x63;
         ok=1;
         continue;
      }
      if(StartWith(l->str,"IF",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+2);
         // expression !!!

         char *p3=strchr(p1,':');
         if(p3==NULL) Error(4,l->str,lines[ln]);
         *p3=0;
         p3=SkipSpace(p3+1);
         k=0;
         int ifok=0;
         if(!k) p2=strchr(p1,'<');
         if(!k&&p2!=NULL) // < <= <>
         {
            *p2=0;p2++;
            if(*p2=='>') // <>
            {
               *p2=0;p2++;
               l->id = 0x15;
               ifok = 1;
            }
            if(!ifok&&*p2=='=') // <=
            {
              *p2=0;p2++;
              l->id = 0x12;
              ifok = 1;
            }
            if(!ifok) l->id = 0x10;
            k=1;
         }
         if(!k) p2=strchr(p1,'>');
         if(!k&&p2!=NULL) // > >=
         {
            *p2=0;p2++;
            if(*p2=='=')
            {
              *p2=0;p2++;
              l->id = 0x13;
            }
            else l->id = 0x11;
            k=1;
         }
         if(!k) p2=strchr(p1,'!');
         if(!k&&p2!=NULL) // !=
         {
            *p2=0;p2++;
            if(*p2!='=') Error(4,l->str,lines[ln]);
            *p2=0;p2++;
            l->id = 0x15;
            k=1;
         }
         if(!k) p2=strchr(p1,'=');
         if(!k&&p2!=NULL) // == =
         {
            *p2=0;p2++;
            if(*p2=='='){*p2=0;p2++;};
            l->id = 0x14;
            k=1;
         }
         if(k==0) Error(4,l->str,lines[ln]);
         //printf("IF (%2.2X) <%s> <%s> ",l->id,p1,p2);
         CompName(&vars,p1,(char*)ttt[ntripl].b1,l->str,0,lines[ln]);
         CompName(&vars,p2,(char*)ttt[ntripl].b2,l->str,0,lines[ln]);
         j = 3;
         if(ttt[ntripl].b1[0]<2) j+=3;
         else j+=5;
         if(ttt[ntripl].b2[0]<2) j+=3;
         else j+=5;
         p1 = strtok(p3," \t");
         l2 = vars.FindFirst(p1,0);
         if(l2==NULL)
         {
            l2 = vars.Add(p1);
            l2->type = -1;
            l2->adr = t->Index(l);
         }
         l->adr = cur;
         ttt[ntripl].b3[0] = 98;
         ttt[ntripl].b3[1] = (l2->adr)%256;
         ttt[ntripl].b3[2] = (l2->adr)/256;
         l->id2 = ntripl;
         l->len = 2;
         cur += j;
         ntripl++;
         if(ntripl>=MAXTRI) Error(99,"TRI>MAXTRI",lines[ln]);
         p2 = strtok(NULL," \t");
         if(p2!=NULL&&*p2!='%') Error(4,l->str,lines[ln]);
         ok=1;
         continue;
      }
      if(StartWith(l->str,"END",1))
      {
         if(!fbody) Error(8,l->str,lines[ln]);
         p1=SkipSpace(p1+3);
         if(*p1!=0&&*p1!='%') Error(4,l->str,lines[ln]);
         l->adr = cur++;
         l->id = 0xFF;
         fend = 1;
         ok=1;
         continue;
      }
      if(!ok) // label or simple expression
      {
         if(StartWith(l->str,"START:",1)) fbody=1;
         if(!fbody) Error(8,l->str,lines[ln]);
         p2=p1;
         while(*p1)
         {
            while(*p1==' '||*p1=='\t')
            {
               p1++;
               if(*p1=='%')
               { *p1=0;
                 p1--;
                 *p1=0;
               }
            }
            *p2 = *p1;
            p1++;
            p2++;
         }
         *p2 = 0;
         j = strlen(str);
         if(str[j-1]==':') // LABEL
         {  l->id = 0; // !!!
            str[j-1]=0;
            k=1;
            for(j=0;j<strlen(str);j++)
            {
               if(!isalpha(str[j])&&!isdigit(str[j])&&str[j]!='_') k=0;
            }
            if(!k) Error(4,l->str,lines[ln]);
            l2 = vars.FindFirst(str);
            while(l2!=NULL)
            {
               if(l2->type!=-1) Error(6,l->str,lines[ln]);
               l3 = t->Get(l2->adr);
               switch(l3->id)
               {
                  case 0x30:
                  case 0x31:
                    l3->id2 = cur;
                    break;
                  default: // IF, TEST
                    if(ttt[l3->id2].b3[0]!=98) Error(99,l->str,lines[ln]);
                    ttt[l3->id2].b3[1] = cur%256;
                    ttt[l3->id2].b3[2] = cur/256;
               }
               l2 = vars.FindNext();
            }
            l2 = vars.Add(str);
            l2->type = 0;
            l2->adr = cur;
         }
         else // Expression !!!
         {
            char name1[20];
            char name2[20];
            char name3[20];
            if(strchr(str,'=')==NULL) Error(4,l->str,lines[ln]);
            k = CompExpr(str,name1,name2,name3);
            l->adr = cur;
            l->id = k;
            l->id2 = ntripl;
            l->len = 3;
            if(name3[0]==0) l->len--;
            else CompName(&vars,name3,(char*)ttt[ntripl].b3,l->str,0,lines[ln]);
            CompName(&vars,name1,(char*)ttt[ntripl].b1,l->str,1,lines[ln]);
            CompName(&vars,name2,(char*)ttt[ntripl].b2,l->str,0,lines[ln]);
            k = 1;
            if(ttt[ntripl].b1[0]<2) k+=3;
            else k+=5;
            if(ttt[ntripl].b2[0]<2) k+=3;
            else k+=5;
            if(name3[0]!=0)
            {
               if(ttt[ntripl].b3[0]<2) k+=3;
               else k+=5;
            }
            cur += k;
            ntripl++;
            if(ntripl>=MAXTRI) Error(99,"TRI>MAXTRI",lines[ln]);
         }
         p1 = str;
      }
   }
   if(fend==0) Error(14,"end of file",lines[ln]);

   #if 0
     printf("\n\nTEXT:\n");
     printf("=====\n");
     t->List();
     printf("\n\nVARS:\n");
     printf("=====\n");
     vars.List();
     printf("\n\n");
   #endif

   for(l=vars.first;l!=NULL;l=l->next)
   {
      if(l->type==-1)
      {
         for(l2=vars.first;l2!=NULL;l2=l2->next)
         {
            if(!strcmp(l->str,l2->str)&&l2->type==0) break;
         }
         if(l2==NULL)
         {  l3 = t->Get(l->adr);
            Error(17,l3->str,l->adr+1);
         }
      }
   }

   code = new unsigned char [cur];
   if(code==NULL) Error(0,"Compile");
   printf("\n name=%s\n",name);
   printf(" var=%u\n",nvar);
   printf(" code=%u\n",cur);

   i = 0;
   ln = -1;
   for(l=t->first;l!=NULL;l=l->next)
   {
      if(l->id==0) continue;
      ln++;
      code[i] = l->id;
      if(code[i]==0x01) // DEF
      {  i++;
         code[i++] = (l->id2)%256;
         code[i++] = (l->id2)/256;
         code[i++] = (l->len)%256;
         code[i++] = (l->len)/256;
         continue;
      }
      if(code[i]==0x02) // DEF 2
      {  i++;
         code[i++] = ttt[l->id2].b1[0];
         code[i++] = ttt[l->id2].b1[1];
         code[i++] = (l->len)%256;
         code[i++] = (l->len)/256;
         code[i++] = ttt[l->id2].b1[2];
         code[i++] = ttt[l->id2].b1[3];
         psh = (short*)ttt[l->id2].str;
         for(j=0;j<(ttt[l->id2].len>>1);j++)
         {
             code[i++] = psh[j]&0xFF;
             code[i++] = (psh[j]>>8)&0xFF;
         }
         continue;
      }
      if(code[i]<0x20) // IF
      {  i++;
         if(ttt[l->id2].b3[0]!=98) Error(99,l->str,lines[ln]);
         code[i++] = ttt[l->id2].b3[1];
         code[i++] = ttt[l->id2].b3[2];
         code[i++] = ttt[l->id2].b1[0];
         code[i++] = ttt[l->id2].b1[1];
         code[i++] = ttt[l->id2].b1[2];
         if(ttt[l->id2].b1[0]==2)
         {
            code[i++] = ttt[l->id2].b1[3];
            code[i++] = ttt[l->id2].b1[4];
         }
         code[i++] = ttt[l->id2].b2[0];
         code[i++] = ttt[l->id2].b2[1];
         code[i++] = ttt[l->id2].b2[2];
         if(ttt[l->id2].b2[0]==2)
         {
            code[i++] = ttt[l->id2].b2[3];
            code[i++] = ttt[l->id2].b2[4];
         }
         continue;
      }
      if(code[i]<0x30) // EXPRESSIONS
      {  i++;
         code[i++] = ttt[l->id2].b1[0];
         code[i++] = ttt[l->id2].b1[1];
         code[i++] = ttt[l->id2].b1[2];
         if(ttt[l->id2].b1[0]==2)
         {
            code[i++] = ttt[l->id2].b1[3];
            code[i++] = ttt[l->id2].b1[4];
         }
         code[i++] = ttt[l->id2].b2[0];
         code[i++] = ttt[l->id2].b2[1];
         code[i++] = ttt[l->id2].b2[2];
         if(ttt[l->id2].b2[0]==2)
         {
            code[i++] = ttt[l->id2].b2[3];
            code[i++] = ttt[l->id2].b2[4];
         }
         if(l->len==3)
         {
            code[i++] = ttt[l->id2].b3[0];
            code[i++] = ttt[l->id2].b3[1];
            code[i++] = ttt[l->id2].b3[2];
            if(ttt[l->id2].b3[0]==2)
            {
               code[i++] = ttt[l->id2].b3[3];
               code[i++] = ttt[l->id2].b3[4];
            }
         }
         else if(l->len!=2) Error(99,l->str,lines[ln]);
         continue;
      }
      if((code[i]>=0x33&&code[i]<=0x36)||code[i]==0x3B||
          code[i]==0x63||code[i]==0xFF) // 1
      {  i++;
         continue;
      }
      if(code[i]==0x37) // SAY
      {  i++;
         p1 = ttt[l->id2].str;
         for(j=0;j<strlen(p1);j++) code[i++]=p1[j];
         code[i++]=0;
//         delete p1;
         continue;
      }
      if(code[i]==0x51) // TEST
      {  i++;
         code[i++] = ttt[l->id2].b3[1];
         code[i++] = ttt[l->id2].b3[2];
         p1 = ttt[l->id2].str;
         for(j=0;j<strlen(p1);j++) code[i++]=p1[j];
         code[i++]=0;
//         delete p1;
         continue;
      }
      if(code[i]==0x3F) // DUMP
      {  i++;
         code[i++]=l->id2;
         if(l->id2==0)
         {
            code[i++]=l->len;
         }
         continue;
      }
      if(code[i]==0x30||code[i]==0x31) // GOTO, CALL
      {  i++;
         code[i++] = (l->id2)%256;
         code[i++] = (l->id2)/256;
         continue;
      }
      if(code[i]==0x61) // SEND
      {  i++;
         code[i++] = ttt[l->id2].b1[0];
         code[i++] = ttt[l->id2].b1[1];
         code[i++] = ttt[l->id2].b1[2];
         if(ttt[l->id2].b1[0]==2)
         {
            code[i++] = ttt[l->id2].b1[3];
            code[i++] = ttt[l->id2].b1[4];
         }
         code[i++] = ttt[l->id2].b2[0];
         code[i++] = ttt[l->id2].b2[1];
         code[i++] = ttt[l->id2].b2[2];
         if(ttt[l->id2].b2[0]==2)
         {
            code[i++] = ttt[l->id2].b2[3];
            code[i++] = ttt[l->id2].b2[4];
         }
         continue;
      }
      if(code[i]==0x38||code[i]==0x3A||code[i]==0x3C|| // PRINT, ACT, RADAR
         code[i]==0x60||code[i]==0x62)  // SHOT, RECV
      {  i++;
         code[i++] = l->type;
         code[i++] = (l->len)%256;
         code[i++] = (l->len)/256;
         if(l->type==2)
         {
            code[i++] = (l->id2)%256;
            code[i++] = (l->id2)/256;
         }
         continue;
      }
      Error(99,"COMMAND",lines[ln]);
   }
   if(cur!=i)
   {
      Error(99,"CUR not eq I");
   }

   #ifdef RDUMP
     printf("\n// DUMP:\n");
     printf("#define RLEN %u\n",cur);
     printf("#define VLEN %u\n",nvar+2);
     printf("unsigned char robot[%u] = {\n",cur);
     for(i=0;i<cur;i++)
     {
        printf(" 0x%2.2X",code[i]);
        if(i!=cur-1) printf(",");
        if(i%10==9) printf("\n");
     }
     printf("\n};\n\n");
   #endif

//   for(i=0;i<MAXTRI;i++) if(ttt[i].str!=NULL) delete ttt[i].str; // TODO: fixit
   delete ttt;
}

short Robot::CompExpr(char *str,char *name1,char *name2,char *name3)
{
   char s[100];
   strcpy(s,str);
   char *p1,*p2;
   p1 = strchr(s,'=');
   *p1=0;p1++;
   strcpy(name1,s);
   if(*p1=='-')
   {
     name3[0]=0;
     strcpy(name2,p1+1);
     return 0x21;
   }
   if(*p1=='~')
   {
     name3[0]=0;
     strcpy(name2,p1+1);
     return 0x2A;
   }
   p2 = strchr(p1,'+');
   if(p2!=NULL)
   {
     *p2=0;p2++;
     strcpy(name2,p1);
     strcpy(name3,p2);
     return 0x22;
   }
   p2 = strchr(p1,'-');
   if(p2!=NULL)
   {
     *p2=0;p2++;
     strcpy(name2,p1);
     strcpy(name3,p2);
     return 0x23;
   }
   p2 = strchr(p1,'/');
   if(p2!=NULL)
   {
     *p2=0;p2++;
     strcpy(name2,p1);
     strcpy(name3,p2);
     return 0x24;
   }
   p2 = strchr(p1,'*');
   if(p2!=NULL)
   {
     *p2=0;p2++;
     strcpy(name2,p1);
     strcpy(name3,p2);
     return 0x25;
   }
   p2 = strchr(p1,'%');
   if(p2!=NULL)
   {
     *p2=0;p2++;
     strcpy(name2,p1);
     strcpy(name3,p2);
     return 0x26;
   }
   p2 = strchr(p1,'&');
   if(p2!=NULL)
   {
     *p2=0;p2++;
     strcpy(name2,p1);
     strcpy(name3,p2);
     return 0x27;
   }
   p2 = strchr(p1,'|');
   if(p2!=NULL)
   {
     *p2=0;p2++;
     strcpy(name2,p1);
     strcpy(name3,p2);
     return 0x28;
   }
   p2 = strchr(p1,'^');
   if(p2!=NULL)
   {
     *p2=0;p2++;
     strcpy(name2,p1);
     strcpy(name3,p2);
     return 0x29;
   }
   strcpy(name2,p1);
   name3[0]=0;
   return 0x20;
}

void Robot::CompName(Text *v,char *pname,char *b,char *line,short n,short il)
{
   int i,j,k;
   Line *l;
   char *po;
   char str[100];
   int nsymbo = 0;
   int ndigit = 0;
   int nalpha = 0;
   char b5[5];
   char *rname = pname;
   while(*rname==' '||*rname=='\t')
   {  rname++;
      if(*rname==0) Error(4,line,il);
   }
   po = rname + strlen(rname) - 1;
   while(*po==' '||*po=='\t'){*po=0;po--;};
   if(strchr(rname,'[')!=NULL) // array element ???
   {
      strcpy(str,rname);
      po = strchr(str,'[');
      if(po!=NULL) *po=0;
      po++;
      k = strlen(po);
      if(po[k-1]!=']') Error(4,line,il);
      po[k-1]=0;
      l = v->FindFirst(str);
      if(l==NULL) Error(10,line,il);
      if(l->type!=2) Error(10,line,il);
      k = l->adr;
      CompName(v,po,(char*)b5,line);
      if(b5[0]>1) Error(4,line,il);
      if(b5[0]==0)
      {
         j = b5[1]+(b5[2]<<8);
         if(j>=l->len) Error(11,line,il);
         b[0] = 1;
         b[1] = (k+j)%256;
         b[2] = (k+j)/256;
      }
      if(b5[0]==1)
      {
         b[0] = 2;
         b[1] = k%256;
         b[2] = k/256;
         b[3] = b5[1];
         b[4] = b5[2];
      }
   }
   else
   {
      for(i=0;i<strlen(rname);i++)
      {
         if(rname[i]=='_') nsymbo++;
         else
         {
            if(isdigit(rname[i])) ndigit++;
            else
            {
               if(isalpha(rname[i])) nalpha++;
               else Error(4,line);
            }
         }
      }
      if(n)
      {
        if(nalpha==0) Error(4,line,il);
        l = v->FindFirst(rname);
        if(l==NULL)
        {
           l = v->Add(rname);
           l->type = 1;
           l->adr = nvar;
           nvar++;
           if(nvar>=VARMAX) Error(9,line,il);
        }
        if(l->type!=1) Error(6,line,il);
        if(l->adr>=0xFF00U) Error(12,line,il);
        b[0] = 1;
        b[1] = (l->adr)%256;
        b[2] = (l->adr)/256;
      }
      else
      {
        if(nalpha==0&&nsymbo==0)
        {  // number
           k = atoi(rname);
           b[0] = 0;
           b[1] = k%256;
           b[2] = k/256;
        }
        else // variable
        {
           l = v->FindFirst(rname);
           if(l==NULL) Error(10,line,il);
           if(l->type!=1) Error(10,line,il);
           b[0] = 1;
           b[1] = (l->adr)%256;
           b[2] = (l->adr)/256;
        }
      }
   }
}

short Robot::Prepro(Text *t)
{
   return t?0:-1;
}

void Robot::Init(void)
{
   equip[0] = 0;
   equip[1] = 0;
   equip[2] = 0;
   equip[3] = 0;
   color = 0xFFFFFFL;
   fimage = 0;
   halt = 0;
   last = NULL;
   X = Y = D = N = K = R = T = S = 0;
   E = ENERGYBEG;
   M = MISSLEBEG;
   elec = 0;
   pc = 0;
   s_i = s_o = 0;
   *name = *author = 0;
   str = new char[STRLEN];
   if(str==NULL) Error(0,"Init 1");
   var = new short[VARMAX];
   if(var==NULL) Error(0,"Init 2");
   for(int i=0;i<VARMAX;i++) var[i]=0;
   sp = VARMAX-1;
   angle = Random(4);
}

void Robot::Save(char *fname)
{
   char *po,str[100];
   strcpy(str,fname);
   po = strrchr(str,'.');
   if(po!=NULL) *po=0;
   strcat(str,".rw0");
   int off,i,j,k;
   unsigned short kc;
   FILE *f = fopen(str,"wb");
   if(f==NULL) Error(1,str);
   off = 0;
   fputc(1,f); // VER 1.99
   fputc(0,f);fputc(0,f);
   fputc(0,f);fputc(0,f);
   off += 5;
   k = strlen(name);
   fputc(k,f);
   for(i=0;i<k;i++) fputc(name[i],f);
   fputc(0,f);
   off += k+2;
   fputc(nvar%256,f);fputc(nvar/256,f);
   fputc(cur%256,f);fputc(cur/256,f);
   off += 4;
   fputc((color>>16)&0xFF,f);
   fputc((color>>8)&0xFF,f);
   fputc(color&0xFF,f);
   off += 3;
   for(i=0;i<4;i++) fputc(equip[i],f);
   off += 4;
   if(fimage)
   {
      fputc(0x20,f);
      for(j=0;j<8;j++){
      for(i=0;i<8;i+=2){
         k = (image[i][j]<<4)|image[i+1][j];
         fputc(k,f);
      }}
      off += 33;
   }
   else
   {
      fputc(0,f);
      off++;
   }
   for(i=0;i<=strlen(author);i++)
   {
      fputc(author[i],f);
      off++;
   }
   fputc(0,f);
   fputc(0,f);
   off += 2;
   for(i=0;i<cur;i++)
   {
      fputc(code[i],f);
   }
   fclose(f);
   f = fopen(str,"rb+");
   if(f==NULL) Error(1,str);
   fseek(f,0L,SEEK_END);
   int fsize = ftell(f);
   fseek(f,5L,SEEK_SET);
   kc = 0;
   for(i=5;i<fsize;i++)
   {
     kc += fgetc(f);
   }
   fseek(f,1L,SEEK_SET);
   fputc(kc%256,f);
   fputc(kc/256,f);
   fputc(off%256,f);
   fputc(off/256,f);
   fclose(f);
}

short Robot::Var(short f)
{
   short i = f;
   unsigned short j;
   long k;
   #ifndef _WITHOUT_INTERPRETER_
   switch(code[pc++])
   {
     case 0:
       i = code[pc++];
       i += code[pc++]<<8;
       break;
     case 1:
       j = code[pc++];
       j += code[pc++]<<8;
       if(j>0xF000U)
       {
          switch(j)
          { case 0xFF00U: i=X; break;
            case 0xFF01U: i=Y; break;
            case 0xFF02U: i=D; break;
            case 0xFF03U: i=N; break;
            case 0xFF04U: i=K; break;
            case 0xFF05U: i=R; break;
            case 0xFF06U: i=T; break;
            case 0xFF07U: i=E; break;
            case 0xFF08U: i=M; break;
            case 0xFF09U: i=number; break; // (I)
            case 0xFF0FU: i=S; break;
          }
          if(f) i=0;
       }
       else
       {
          if(j>=VARMAX) return 0;
          if(f) i=j;
          else i=var[j];
       }
       break;
     case 2:
       i = code[pc++];
       i += code[pc++]<<8;
       j = code[pc++];
       j += code[pc++]<<8;
       if(j>0xF000U)
       {
          switch(j)
          { case 0xFF00U: k=(long)i+X; break;
            case 0xFF01U: k=(long)i+Y; break;
            case 0xFF02U: k=(long)i+D; break;
            case 0xFF03U: k=(long)i+N; break;
            case 0xFF04U: k=(long)i+K; break;
            case 0xFF05U: k=(long)i+R; break;
            case 0xFF06U: k=(long)i+T; break;
            case 0xFF07U: k=(long)i+E; break;
            case 0xFF08U: k=(long)i+M; break;
            case 0xFF0FU: k=(long)i+S; break;
          }
       }
       else
       {
          if(j>=VARMAX) return 0;
          k = (long)i+var[j];
       }
       if(k<0||k>=VARMAX) return 0;
       if(f) i=k;
       else i=var[k];
       break;
   }
   #endif
   return i;
}

short Robot::Adr(void)
{
   short i;
   i = code[pc++];
   i += code[pc++]<<8;
   return i;
}

char* Robot::Step(FILE *f)
{
   short i,j,k,k2,k3,xx,yy,dd,lastcmd,nfree=NFREE;
 freeloop:
   char *po = NULL;
   #ifndef _WITHOUT_INTERPRETER_
   R = Random(1000);
   command = 0;
   if(E<=0) return NULL;
   if((xr>0&&map[xr-1][yr]==0x40)||
      (yr>0&&map[xr][yr-1]==0x40)||
      (xr<dx-1&&map[xr+1][yr]==0x40)||
      (yr<dy-1&&map[xr][yr+1]==0x40))
   {
      elec++;
      if(elec>=ENERGWAIT)
      {  elec = 0;
         if(E>0) E++;
         if(E>ENERGYMAX) E=ENERGYMAX;
      }
   }
   else elec=0;
   if(pc>=cur) halt=1;
   if(halt) return NULL;
   lastcmd = code[pc++];
   switch(lastcmd)
   {
     case 0xFF:
          halt=1;
          break;
     case 0x00:
          break;
     case 0x01:
          i = Adr();
          j = Adr();
          break;
     case 0x02:
          i = Adr();
          j = Adr();
          k2 = Adr();
          for(k=0;k<k2;k++) var[i+k]=Adr();
          break;
     case 0x10:
          k = Adr();
          i = Var();
          j = Var();
          if(i<j) pc=k;
          break;
     case 0x11:
          k = Adr();
          i = Var();
          j = Var();
          if(i>j) pc=k;
          break;
     case 0x12:
          k = Adr();
          i = Var();
          j = Var();
          if(i<=j) pc=k;
          break;
     case 0x13:
          k = Adr();
          i = Var();
          j = Var();
          if(i>=j) pc=k;
          break;
     case 0x14:
          k = Adr();
          i = Var();
          j = Var();
          if(i==j) pc=k;
          break;
     case 0x15:
          k = Adr();
          i = Var();
          j = Var();
          if(i!=j) pc=k;
          break;
     case 0x20:
          k = Var(1);
          i = Var();
          var[k] = i;
          break;
     case 0x21:
          k = Var(1);
          i = Var();
          var[k] = -i;
          break;
     case 0x22:
          k = Var(1);
          i = Var();
          j = Var();
          var[k] = i+j;
          break;
     case 0x23:
          k = Var(1);
          i = Var();
          j = Var();
          var[k] = i-j;
          break;
     case 0x24:
          k = Var(1);
          i = Var();
          j = Var();
          if(j) var[k] = i/j;
          else  var[k] = 32767;
          break;
     case 0x25:
          k = Var(1);
          i = Var();
          j = Var();
          var[k] = i*j;
          break;
     case 0x26:
          k = Var(1);
          i = Var();
          j = Var();
          if(j) var[k] = i%j;
          else  var[k] = 0;
          break;
     case 0x27:
          k = Var(1);
          i = Var();
          j = Var();
          var[k] = i&j;
          break;
     case 0x28:
          k = Var(1);
          i = Var();
          j = Var();
          var[k] = i|j;
          break;
     case 0x29:
          k = Var(1);
          i = Var();
          j = Var();
          var[k] = i^j;
          break;
     case 0x2A:
          k = Var(1);
          i = Var();
          var[k] = ~i;
          break;
     case 0x30:
          k = Adr();
          pc = k;
          break;
     case 0x31:
          k = Adr();
          var[sp--] = pc;
          if(sp<nvar) halt=1;
          pc = k;
          break;
     case 0x33:
          if(sp<VARMAX-1) sp++;
          else halt=1;
          pc = var[sp];
          break;
     case 0x34:
          i = xr;
          j = yr;
          N = 0;
          switch(angle)
          {
             case 0: i++; break;
             case 1: j--; break;
             case 2: i--; break;
             case 3: j++; break;
          }
          if(i<0||i>=dx){N=10;break;}
          if(j<0||j>=dy){N=10;break;}
          switch(map[i][j]>>4)
          {
             case 1:
                  command=0x400+number-1;
                  break;
             case 0:
             case 5:
             case 3:
                  map[xr][yr] = 0;
                  xr = i;
                  yr = j;
                  if((map[i][j]>>4)==3) M+=MISSLEADD;
                  map[xr][yr]=(6<<4)+number-1;
                  break;
             case 2:
                  N = 2;
                  break;
             case 4:
                  N = 4;
                  break;
             case 6:
                  N = 6;
                  break;
          }
          break;
     case 0x35:
          angle++;
          if(angle>3) angle-=4;
          break;
     case 0x36:
          angle--;
          if(angle<0) angle+=4;
          break;
     case 0x37:
          i = 0;
          j = code[pc++];
          while(j!=0)
          {  if(i<99) str[i++] = j;
             j = code[pc++];
          }
          str[i] = 0;
          po = str;
          if(last!=NULL) strcpy(last,po);
          if(f!=NULL) fprintf(f,"T=%u\tROBOT%u '%s'\tSAY <%s>\n",T,number,name,str);
          break;
     case 0x38:
          i = Var();
          sprintf(str,"%i",i);
          po = str;
          if(f!=NULL) fprintf(f,"T=%u\tROBOT%u '%s'\tPRINT %s\n",T,number,name,str);
          break;
     case 0x3A:
          i = Var();
          j = equip[i&3];
          k = angle-(i&3);
          if(k<0) k+=4;
          if(k>3) k-=4;
          if(j==1)
          {
             D = 0;
             xx = xr;
             yy = yr;
             command = 0x200+k;
             while(1)
             {   D++;K=0;
                 switch(k)
                 {
                    case 0: xx++; break;
                    case 1: yy--; break;
                    case 2: xx--; break;
                    case 3: yy++; break;
                 }
                 if(xx<0||xx>=dx||yy<0||yy>=dy){N=10;break;}
                 N = map[xx][yy]>>4;
                 if(N==6)
                 {
                    i = map[xx][yy]&0xF;
                    K = rar[i]->E;
                    if(f_gr)
                    {
                       if(!strcmp(author,rar[i]->GetAuthor())) N=7;
                    }
                 }
                 if(N==2) K=map[xx][yy]&0xF;
                 if(N==5)
                 {  i=map[xx][yy]&0xF;
                    if(xx==xr)
                    {
                       if(yy>yr)
                       {
                          if(i==1) K=1;
                       }
                       else
                       {
                          if(i==3) K=1;
                       }
                    }
                    if(yy==yr)
                    {
                       if(xx>xr)
                       {
                          if(i==2) K=1;
                       }
                       else
                       {
                          if(i==0) K=1;
                       }
                    }
                 }
                 if(N>=1) break;
             }
          }
          if(j==2&&M>0)
          {
             M--;
             command = 0x100 + k;
          }
          break;
     case 0x3B:
          command = 0x300;
          break;
     case 0x3C:
          k2 = Var();
          k3 = 0;
          if(f_gr&&k2==7)
          {  k2 = 6;
             k3 = 1;
          }
          k = angle;
          while(k<0) k+=4;
          while(k>3) k-=4;
          dd = dx+dy+2;
          E--;
          X = Y = 0;
          for(i=0;i<dx;i++){
          for(j=0;j<dy;j++){
             if(k2==(map[i][j]>>4))
             {
                if(k2==6&&i==xr&&j==yr) continue;
                if(k3 && strcmp(author,rar[map[i][j]&0xF]->GetAuthor()))
                   continue;
                xx=xr-i;if(xx<0)xx=-xx;
                yy=yr-j;if(yy<0)yy=-yy;
                if(xx+yy<dd)
                {
                   switch(k)
                   {
                     case 0: X=j-yr; Y=i-xr; break;
                     case 1: X=i-xr; Y=yr-j; break;
                     case 2: X=yr-j; Y=xr-i; break;
                     case 3: X=xr-i; Y=j-yr; break;
                   }
                   dd = xx+yy;
                }
             }
          }}
          xx = X;
          yy = Y;
          if(xx<0) xx=-xx;
          if(yy<0) yy=-yy;
          if(xx<yy) D=xx;
          else D=yy;
          if(X>=0&&Y>=0) K=0;
          if(X<0&&Y>=0) K=1;
          if(X<0&&Y<0) K=2;
          if(X>=0&&Y<0) K=3;
          break;
     case 0x3F:
          i = code[pc++];
          j = code[pc++];
          if(i!=0){halt=1;break;}
          sprintf(str,"DUMP %i",j);
          po = str;
          if(f!=NULL)
          {
             fprintf(f,"T=%u\tROBOT%u '%s':\n",T,number,name);
             for(i=0;i<nvar;i++)
             {
                if(i%j==0) fprintf(f," ");
                fprintf(f,"%i ",var[i]);
                if(i%j==j-1||i==nvar-1) fprintf(f,"\n");
             }
          }
          break;
     case 0x51:
          k = Adr();
          i = 0;
          j = code[pc++];
          while(j!=0)
          {  if(i<99) str[i++] = j;
             j = code[pc++];
          }
          str[i] = 0;
          i = WildCmp(last,str);
          if(i) pc=k;
          if(f!=NULL) fprintf(f,"T=%u\tROBOT%u '%s'\tTEST '%s'%c='%s'\n",
                                 T,number,name,str,i?'=':'!',last);
          break;
     case 0x60:
          i = Var(1);
          if(i<VARMAX) var[i]=X; else break; i++; // 0
          if(i<VARMAX) var[i]=Y; else break; i++; // 1
          if(i<VARMAX) var[i]=D; else break; i++; // 2
          if(i<VARMAX) var[i]=N; else break; i++; // 3
          if(i<VARMAX) var[i]=K; else break; i++; // 4
          if(i<VARMAX) var[i]=R; else break; i++; // 5
          if(i<VARMAX) var[i]=T; else break; i++; // 6
          if(i<VARMAX) var[i]=E; else break; i++; // 7
          if(i<VARMAX) var[i]=M; else break; i++; // 8
          if(i<VARMAX) var[i]=number; else break; i++; // 9 (I)
          if(i<VARMAX) var[i]=0; else break; i++; // 10
          if(i<VARMAX) var[i]=0; else break; i++; // 11
          if(i<VARMAX) var[i]=0; else break; i++; // 12
          if(i<VARMAX) var[i]=0; else break; i++; // 13
          if(i<VARMAX) var[i]=0; else break; i++; // 14
          if(i<VARMAX) var[i]=S; else break; i++; // 15
          break;
     case 0x61:
          send = Var();
          k = Var()&0xFF;
          command = 0x500 + k;
          E--;
          break;
     case 0x62:
          k2 = Var(1);
          if(s_i==s_o)
          {  var[k2] = 0;
             N = X = Y = K = 0;
          }
          else
          {  var[k2] = s_k[s_o];
             N = s_n[s_o];
             K = s_t[s_o];
             i = s_x[s_o];
             j = s_y[s_o];
             k = angle;
             while(k<0) k+=4;
             while(k>3) k-=4;
             switch(k)
             {
               case 0: X=j-yr; Y=i-xr; break;
               case 1: X=i-xr; Y=yr-j; break;
               case 2: X=yr-j; Y=xr-i; break;
               case 3: X=xr-i; Y=j-yr; break;
             }
             s_o++;
             if(s_o>=SENDBUF) s_o=0;
          }
          if(f!=NULL) fprintf(f,"T=%u\tROBOT%u '%s'\tRECV %i %i (%i,%i) T=%i [%i]\n",
                                 T,number,name,var[k2],N,X,Y,K,k2);
          break;
     case 0x63:
          sp++;
          if(sp>=VARMAX) sp=VARMAX-1;
          break;
     default:
          halt=1;
   }
   S = sp - VARMAX + 1;
   if( lastcmd==0x00 ||
       lastcmd==0x01 ||
       lastcmd==0x30 ||
       lastcmd==0x31 ||
       lastcmd==0x33
     )
   {
     if(--nfree) goto freeloop;
   }
   T++;
   #endif
   return po;
}

void Robot::SetImage(int f,void *pv)
{
   int i,j,k;
   if(f==0||pv==NULL)
   { fimage = 0;
     return;
   }
   if(f==1)
   { char *im = (char*)pv;
     for(j=0;j<8;j++){
     for(i=0;i<8;i++){
        image[i][j] = *im;
        im++;
     }}
     fimage=1;
     return;
   }
   if(f!=2) return;
   char **im2 = (char**)pv;
   for(j=0;j<8;j++){
   for(i=0;i<8;i++){
      image[i][j] = im2[i][j];
   }}
   fimage=1;
}

void Robot::SetName(char *n)
{
   if(strlen(n)>15) Error(3,n);
   strcpy(name,n);
}

void Robot::SetAuthor(char *n)
{
   if(strlen(n)>15) Error(3,n);
   strcpy(author,n);
}

short Robot::Send(short k,short n,short t,short x,short y)
{
   s_k[s_i] = k;
   s_n[s_i] = n;
   s_t[s_i] = t;
   s_x[s_i] = x;
   s_y[s_i] = y;
   s_i++;
   if(s_i==SENDBUF) s_i=0;
   if(s_i==s_o)
   {  s_o++;
      if(s_o==SENDBUF) s_o=0;
      return 0;
   }
   return 1;
}

double Rand = 0.54321;

short Random(short n)
{
   Rand = 11*Rand + 3.14159265358979323846;
   Rand = Rand - (int)Rand;
   return Rand*n;
}

short _M_reaktor = 2;
short _M_yama    = 2;
short _M_yaschik = 5;
short _M_kamen   = 20;
short _X_box[DMBOX];
short _Y_box[DMBOX];
short _N_box = 0;

unsigned char ** MakeMap(short n,short x,short y)
{
   short dx,dy,i,j,k,t;
   dx = x;
   dy = y;
   double s = 100.0*n;
   k = 10*sqrt((double)n);
   if(dx==0&&dy==0) dx=dy=k;
   if(dy==0) dy=s/dx;
   int n2 = n;
   if(s>dx*dy) n2=2;
   printf("\n Make MAP %u x %u\n",dx,dy);
   short reaktor = n2*_M_reaktor;
   short yama    = n2*_M_yama;
   short yaschik = n2*_M_yaschik;
   short kamen   = n2*_M_kamen;
   unsigned char **map;
   map = new unsigned char*[dx];
   if(map==NULL) Robot::Error(0,"MakeMap");
   for(i=0;i<dx;i++)
   {
      map[i] = new unsigned char[dy];
      if(map[i]==NULL) Robot::Error(0,"MakeMap2");
      for(j=0;j<dy;j++) map[i][j]=0;
   }
   i = j = 0;
   for(k=0;k<kamen;k++)
   {
      while(map[i][j])
      {
        i = Random(dx);
        j = Random(dy);
      }
      map[i][j] = 0x25;
   }
   for(k=0;k<yama;k++)
   {
      while(map[i][j])
      {
        i = Random(dx);
        j = Random(dy);
      }
      map[i][j] = 0x10;
   }
   for(k=0;k<yaschik;k++)
   {
      while(map[i][j])
      {
        i = Random(dx);
        j = Random(dy);
      }
      map[i][j] = 0x30;
      if(_N_box<DMBOX)
      {
         _X_box[_N_box]=i;
         _Y_box[_N_box]=j;
         _N_box++;
      }
   }
   for(k=0;k<reaktor;k++)
   {
      while(map[i][j])
      {
        i = Random(dx);
        j = Random(dy);
      }
      map[i][j] = 0x40;
   }
   for(k=0;k<n;k++)
   {
      while(map[i][j])
      {
        i = Random(dx);
        j = Random(dy);
      }
      map[i][j] = 0x60 + k;
   }
   return map;
}

