rw1o
====

This is "Robot Warfare 1 Open Game" version 1.99.3

Compatible with shareware game "Robot Warfare 1" version 1.99 (2000)

Copyright (c) 1998-2013 A.A.Shabarshin <me@shaos.net>

Initially opensourced in 2001 and relicensed under MIT license (see file LICENSE) in 2013

https://gitlab.com/shaos/rw1o

For newer source code see other repo:

https://gitlab.com/shaos/rw2d
