/*
 Robot Warfare 1 Open Game
 =========================

 Copyright (c) 1998-2013 A.A.Shabarshin <me@shaos.net>

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom
 the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

// RW1_DUEL.CPP - Shabarshin A.A.  12.11.1998, 10.11.2001, 21.02.2013

#ifndef linux
#ifndef SDL
#ifndef BORLAND
#include <i86.h>
#endif
#endif
#include <conio.h>
#endif
#include <time.h>
#include "robotwar.h"
#ifdef BORLAND
#include "my_vga.hpp"
#else
#ifdef SDL
#include "my_vgas.hpp"
#else
#ifndef linux
#include "my_vgaw.hpp"
#else
#include "my_vgax.hpp"
// you need svgalib and root rights for using it under linux
#define getch vga_getch
#define kbhit() 0 // you need ctrl-c for finish under linux
#define sound(x)
#define nosound()
#define delay(x) mydelay(x)
void mydelay(int x)
{int t=clock();
 int tt=t+x/1000.0*CLOCKS_PER_SEC;
 while(clock()<tt);
}
#endif
#endif
#endif
#include "txr_into.hpp"

#define FULLDEB
//#define MAPDRAW

int NUM = 2;
#define MNUM 20

#define SIZE     16
#define SBIT     4
#define DDX      20
#define DDY      10
#define NOREG    2000
#define TCOL     215
#define NPOV     4
#define FIRES    50
#define FIREF    23
#define SSIZE    8000
#define TIMEMAX  10000
#define TIMELIM  200
#define TIMEDM   1000
#define SNUM     3
#define DELA     56
#define NBUILT   4
#define ARGCMAX  100
#define STARTE   176
#define FILEOUT  "RW1_DUEL.OUT"

#define BYTE unsigned char
#define USHORT unsigned short

int rtime = 0;
short dx,dy;
unsigned char **map;
Robot *r[MNUM];
Palette256 *dac;

#include "builtin.h"

char robotname[NBUILT][20] = {"WINSIMPLE","BUILT IN 2","MONSTER 7","TANK I"};
int rlen[NBUILT] = {RLEN0,RLEN1,RLEN2,RLEN3};
int vlen[NBUILT] = {VLEN0,VLEN1,VLEN2,VLEN3};

char rwtitle[] = "Robot Warfare 1 DUEL 1.99open Shabarshin";

int f_s = 1; // sound
int f_f = 0; // fast
int f_n = 0; // robot number
int f_i = 0; // stat
int f_r = 1; // builtin
int f_g = 0; // goto
int f_d = 0; // debug
int f_gr = 0; // group
int f_dm = 0; // deathmatch

struct group
{
   char author[16];
   short emax,mmax,num;
};

group gr[MNUM];
short ngr = 0;

struct mstru
{
   short n;
   short x;
   short y;
};

mstru ms[MNUM];

struct sstru
{
  USHORT time;
  USHORT all_shoot;
  USHORT good_shoot;
  USHORT kill_shoot;
  USHORT min_missle;
  USHORT max_missle;
  USHORT fin_missle;
  USHORT min_energy;
  USHORT max_energy;
  USHORT fin_energy;
  USHORT boxes;
  USHORT damages;
  USHORT reason;
};

sstru rwstat[MNUM];

Texture tempty  (  EMPTY_width,  EMPTY_height,  EMPTY_bytes);
Texture thole   (   HOLE_width,   HOLE_height,   HOLE_bytes);
Texture tstone  (  STONE_width,  STONE_height,  STONE_bytes);
Texture tbox    (    BOX_width,    BOX_height,    BOX_bytes);
Texture treactor(REACTOR_width,REACTOR_height,REACTOR_bytes);
Texture tmissle0(MISSLE0_width,MISSLE0_height,MISSLE0_bytes);
Texture tmissle1(MISSLE1_width,MISSLE1_height,MISSLE1_bytes);
Texture tmissle2(MISSLE2_width,MISSLE2_height,MISSLE2_bytes);
Texture tmissle3(MISSLE3_width,MISSLE3_height,MISSLE3_bytes);
Texture tfire   (   FIRE_width,   FIRE_height,   FIRE_bytes);
Texture trobot[MNUM];

void Sound(short i,short d=10);
void Fire1(short i,short j,short k,short kr);
void Fire2(short i,short j,short k,short kr);
void RShow(short k);

BYTE povx[NPOV][16][16];
BYTE povy[NPOV][16][16];

int rstep[MNUM];

int mescolor = 129;

void Message(char *s,int k=-1)
{
  if(f_f==2) return;
  int i,j,ok=0;
  char str[32],st2[32];
  if(k!=-1&&*s!=0)
  {
     strncpy(st2,s,30);
     if(strlen(st2)>=24) s[23]=0;
     sprintf(str,"ROBOT%u:%s",k+1,st2);
  }
  else strncpy(str,s,30);
  str[30]=0;
  for(i=0;i<30;i++)
  {
     if(str[i]==0) ok=1;
     if(ok) DrawChar(8*i,172,' ',130,134);
     else DrawChar(8*i,172,str[i],mescolor,134);
  }
}

void delayf(int n)
{
  if(!f_f) delay(n);
}

void InitPov(void)
{
  int i,j,ii,jj,k,x,y;
  if(f_f==2) return;
  double a,c,s;
  for(k=0;k<NPOV;k++)
  {  a = M_PI/2.0/(NPOV+1)*(k+1);
     c = cos(a);
     s = sin(a);
     for(i=0;i<16;i++){ ii=i-8;
     for(j=0;j<16;j++){ jj=j-8;
       x = ii*c-jj*s+8;
       y = ii*s+jj*c+8;
       if(x<0||y<0||x>=16||y>=16) x=y=0;
       povx[k][i][j] = x;
       povy[k][i][j] = y;
     }}
  }
}

void LeftPov(short n,short k)
{
   int i,j,x,y,b;
   if(f_f==2) return;
   x = (r[n]->GetX())<<SBIT;
   y = (r[n]->GetY())<<SBIT;
   if(k<NPOV)
   {
     for(i=0;i<16;i++){
     for(j=0;j<16;j++){
       b = trobot[n].GetPixel(povx[k][i][j],povy[k][i][j]);
       if(b!=0) PutScreenPixel(x+i,y+j,b);
       else PutScreenPixel(x+i,y+j,tempty.GetPixel(i,j));
     }}
   }
   else
   {
     trobot[n].Left();
     RShow(n);
   }
}

void RightPov(short n,short k)
{
   int i,j,x,y,b;
   if(f_f==2) return;
   x = (r[n]->GetX())<<SBIT;
   y = (r[n]->GetY())<<SBIT;
   if(k==0) trobot[n].Right();
   if(k<NPOV)
   {
     for(i=0;i<16;i++){
     for(j=0;j<16;j++){
       b = trobot[n].GetPixel(povx[NPOV-k-1][i][j],povy[NPOV-k-1][i][j]);
       if(b!=0) PutScreenPixel(x+i,y+j,b);
       else PutScreenPixel(x+i,y+j,tempty.GetPixel(i,j));
     }}
   }
   else
   {
     RShow(n);
   }
}

void Sound(short i,short d)
{
   if(f_f==2) return;
   if(f_s==0)
   {
      delayf(d);
   }
   if(f_s==1)
   {
      sound(i);
      delayf(d);
      nosound();
   }
}

void RShow(short k)
{
   int x,y;
   if(f_f==2) return;
   x = (r[k]->GetX())<<SBIT;
   y = (r[k]->GetY())<<SBIT;
   tempty.Draw(x,y);
   if(r[k]->E>0) trobot[k].Draw(x,y);
}

#define NUMCOM 1000

short ncom,ncom2;
short command[NUMCOM];
short comx[NUMCOM];
short comy[NUMCOM];
short comk[NUMCOM];
short comn[NUMCOM];
short comt[NUMCOM];
short comd[NUMCOM];

void RoboDead(int nn,int kk)
{
  if(r[nn]->E<=0) return;
  for(int ii=0;ii<kk;ii++)
  {
     if(r[nn]->Damage()<=0)
     {
        map[r[nn]->GetX()][r[nn]->GetY()] = 0;
        break;
     }
  }
}

#define KFIRE1 4
#define KFIRE2 6

void Fire1(short i,short j,short k,short kr)
{
  short ii,jj,kk,nn;
  short x = i<<SBIT;
  short y = j<<SBIT;
  switch(k)
  {
    case 0:
      map[i][j]=0;
      if(f_f!=2) tfire.Draw(x,y);
      break;
    case 1:
      if(f_f!=2)
      {
       if(y>=SIZE)   tfire.Draw(x,y-SIZE);
       if(x<dx-SIZE) tfire.Draw(x+SIZE,y);
       if(y<dy-SIZE) tfire.Draw(x,y+SIZE);
       if(x>=SIZE)   tfire.Draw(x-SIZE,y);
      }
      break;
    case 2:
      if(f_f!=2)
      {
       if(x>=SIZE&&y>=SIZE)     tfire.Draw(x-SIZE,y-SIZE);
       if(x<dx-SIZE&&y>=SIZE)   tfire.Draw(x+SIZE,y-SIZE);
       if(x<dx-SIZE&&y<dy-SIZE) tfire.Draw(x+SIZE,y+SIZE);
       if(x>=SIZE&&y<dy-SIZE)   tfire.Draw(x-SIZE,y+SIZE);
      }
      break;
    case 3:
      for(x=-1;x<=1;x++){ ii=i+x;
      for(y=-1;y<=1;y++){ jj=j+y;
          if(ii>=0&&ii<DDX&&jj>=0&&jj<DDY)
          {  if((map[ii][jj]>>4)==6)
             {
               for(nn=0;nn<NUM;nn++)
               {
                if(r[nn]->GetX()==ii&&r[nn]->GetY()==jj)
                {
                   RoboDead(nn,3);
                   RShow(nn);
                   rwstat[kr].good_shoot++;
                   if(r[nn]->E<=0)
                   {  rwstat[kr].kill_shoot++;
                      rwstat[nn].reason = 3;
                   }
                   rwstat[nn].damages+=3;
                }
               }
             }
             else
             {
                if((map[ii][jj]>>4)==3)
                {  command[ncom]=0x101;
                   comk[ncom] = NPOV;
                   comd[ncom] = 0;
                   comn[ncom] = kr;
                   comx[ncom] = ii;
                   comy[ncom++] = jj;
                }
                if((map[ii][jj]>>4)==4)
                {  command[ncom]=0x102;
                   comk[ncom] = NPOV;
                   comd[ncom] = 0;
                   comn[ncom] = kr;
                   comx[ncom] = ii;
                   comy[ncom++] = jj;
                }
                if((map[ii][jj]>>4)!=1)
                {  map[ii][jj]=0;
                   if(f_f!=2) tempty.Draw(ii<<SBIT,jj<<SBIT);
                }
                else if(f_f!=2) thole.Draw(ii<<SBIT,jj<<SBIT);

             }
          }
      }}
      break;
  }
  if(f_f!=2) TraceWait();
}



void Fire2(short i,short j,short k,short kr)
{
  short ii,jj,kk,nn;
  short x = i<<SBIT;
  short y = j<<SBIT;
  short siz2 = SIZE<<1;
  switch(k)
  {
    case 0:
      map[i][j]=1<<4;
      if(f_f!=2)
      {
       tfire.Draw(x,y);
       if(y>=SIZE)   tfire.Draw(x,y-SIZE);
       if(x<dx-SIZE) tfire.Draw(x+SIZE,y);
       if(y<dy-SIZE) tfire.Draw(x,y+SIZE);
       if(x>=SIZE)   tfire.Draw(x-SIZE,y);
      }
      break;
    case 1:
      if(f_f!=2)
      {
       if(x>=SIZE&&y>=SIZE)     tfire.Draw(x-SIZE,y-SIZE);
       if(x<dx-SIZE&&y>=SIZE)   tfire.Draw(x+SIZE,y-SIZE);
       if(x<dx-SIZE&&y<dy-SIZE) tfire.Draw(x+SIZE,y+SIZE);
       if(x>=SIZE&&y<dy-SIZE)   tfire.Draw(x-SIZE,y+SIZE);
      }
      break;
    case 2:
      if(f_f!=2)
      {
       if(x>=siz2)   tfire.Draw(x-siz2,y);
       if(y>=siz2)   tfire.Draw(x,y-siz2);
       if(x<dx-siz2) tfire.Draw(x+siz2,y);
       if(y<dy-siz2) tfire.Draw(x,y+siz2);
      }
      break;
    case 3:
      for(ii=-2;ii<=2;ii++){
      for(jj=-2;jj<=2;jj++){
         x = ii+i;
         y = jj+j;
         if(x>=0&&y>=0&&x<DDX&&y<DDY&&f_f!=2) tfire.Draw(x<<SBIT,y<<SBIT);
      }}
      break;
    case 4:
      for(x=-2;x<=2;x++){ ii=i+x;
      for(y=-2;y<=2;y++){ jj=j+y;
          if(ii>=0&&ii<DDX&&jj>=0&&jj<DDY)
          {  if((map[ii][jj]>>4)==6)
             {
               for(nn=0;nn<NUM;nn++)
               {
                if(r[nn]->GetX()==ii&&r[nn]->GetY()==jj)
                {
                   RoboDead(nn,5);
                   RShow(nn);
                   rwstat[kr].good_shoot++;
                   if(r[nn]->E<=0)
                   {  rwstat[kr].kill_shoot++;
                      rwstat[nn].reason = 5;
                   }
                   rwstat[nn].damages+=5;

                }
               }
             }
             else
             {
                if((map[ii][jj]>>4)==3)
                {  command[ncom]=0x101;
                   comk[ncom] = NPOV;
                   comd[ncom] = 0;
                   comn[ncom] = kr;
                   comx[ncom] = ii;
                   comy[ncom++] = jj;
                }
                if((map[ii][jj]>>4)==4)
                {  command[ncom]=0x102;
                   comk[ncom] = NPOV;
                   comd[ncom] = 0;
                   comn[ncom] = kr;
                   comx[ncom] = ii;
                   comy[ncom++] = jj;
                }
                if((map[ii][jj]>>4)!=1)
                { map[ii][jj]=0;
                  if(f_f!=2) tempty.Draw(ii<<SBIT,jj<<SBIT);
                }
                else if(f_f!=2) thole.Draw(ii<<SBIT,jj<<SBIT);
             }
          }
      }}
      break;
  }
  if(f_f!=2) TraceWait();
}


void SetGamePalette(void)
{
  int i;
  for(i=0;i<192;i++) dac->SetPalette(i,RGB(0,0,0));
  for(i=192;i<256;i++) dac->SetPalette(i,RGB(i-192,i-192,i-192));
  int h0=0;
  int h1=16;
  int h2=32;
//  int h3=48;
  int h4=63;
  int c0=0;
  int c1=21;
  int c2=42;
  int c3=63;
  dac->SetPalette(1,RGB(c0,c0,c2));
  dac->SetPalette(2,RGB(c0,c2,c0));
  dac->SetPalette(3,RGB(c0,c2,c2));
  dac->SetPalette(4,RGB(c2,c0,c0));
  dac->SetPalette(5,RGB(c2,c0,c2));
  dac->SetPalette(6,RGB(c2,c1,c0));
  dac->SetPalette(7,RGB(c2,c2,c2));
  dac->SetPalette(8,RGB(c1,c1,c1));
  dac->SetPalette(9,RGB(c0,c0,c3));
  dac->SetPalette(10,RGB(c0,c3,c0));
  dac->SetPalette(11,RGB(c0,c3,c3));
  dac->SetPalette(12,RGB(c3,c0,c0));
  dac->SetPalette(13,RGB(c3,c0,c3));
  dac->SetPalette(14,RGB(c3,c3,c0));
  dac->SetPalette(15,RGB(c3,c3,c3));
  int step=4;
  int r,g,b;
  r=h0;
  g=h0;
  b=h4;
  for(i=0x10;i<0x20;i++)
  {
      dac->SetPalette(i,RGB(r,g,b));
      r+=3;
      g+=3;
  }
  r=c0;
  g=c0;
  b=h1;
  for(i=0x20;i<0x30;i++)
  {
      dac->SetPalette(i,RGB(r,g,b));
      b+=3;
  }
  r=c0;
  g=8;
  b=c0;
  for(i=0x30;i<0x40;i++)
  {
      dac->SetPalette(i,RGB(r,g,b));
      g+=2;
  }
  r=8;
  g=8;
  b=h0;
  for(i=0x40;i<0x50;i++)
  {
      dac->SetPalette(i,RGB(r,g,b));
      g+=1;
      r+=2;
  }
  r=h2;
  g=h1;
  b=h0;
  for(i=0x50;i<0x60;i++)
  {
      dac->SetPalette(i,RGB(r,g,b));
      g+=3;
      r+=2;
  }
  r=h0;
  g=h0;
  b=h0;
  for(i=0x60;i<0x70;i++)
  {
      dac->SetPalette(i,RGB(r,g,b));
      g+=step;
      b+=step;
  }
  r=h1;
  g=h4;
  b=h4;
  for(i=0x70;i<0x80;i++)
  {
      dac->SetPalette(i,RGB(r,g,b));
      r+=3;
  }
  r=h1;
  g=h0;
  b=h1;
  for(i=0x80;i<0x90;i++)
  {
      dac->SetPalette(i,RGB(r,g,b));
      r+=3;
      b+=3;
  }
  r=h1;
  g=h0;
  b=h0;
  for(i=0x90;i<0xA0;i++)
  {
      dac->SetPalette(i,RGB(r,g,b));
      r+=3;
  }
  r=h4;
  g=h0;
  b=h0;
  for(i=0xA0;i<0xB0;i++)
  {
      dac->SetPalette(i,RGB(r,g,b));
      g+=step;
  }
  r=h4;
  g=h4;
  b=h0;
  for(i=0xB0;i<0xC0;i++)
  {
      dac->SetPalette(i,RGB(r,g,b));
      b+=step;
  }
  dac->SetHardPalette();
}

short NearColor(short k)
{
  int i,j,i1,i2,i3,i4,rr,gg,bb,e=9999;
  rr = r[k]->GetR();
  gg = r[k]->GetG();
  bb = r[k]->GetB();
  j=255;
  for(i=0;i<256;i++)
  {
     RGB rgb = dac->GetPalette(i);
     i1=rr-rgb.r;if(i1<0)i1=-i1;
     i2=gg-rgb.g;if(i2<0)i2=-i2;
     i3=bb-rgb.b;if(i3<0)i3=-i3;
     i4=i1;
     if(i2>i4)i4=i2;
     if(i3>i4)i4=i3;
     if(i4<e)
     { e=i4;
       j=i;
     }
  }
  return j;
}

void htrace(int x,int y,int xk)
{
  if(f_f==2) return;
  for(int i=x;i<=xk;i+=2)
  {
    PutScreenPixel(i,y+3,TCOL);
    PutScreenPixel(i,y+4,TCOL);
    PutScreenPixel(i,y+11,TCOL);
    PutScreenPixel(i,y+12,TCOL);
  }
}

void vtrace(int x,int y,int yk)
{
  if(f_f==2) return;
  for(int j=y;j<=yk;j+=2)
  {
    PutScreenPixel(x+3,j,TCOL);
    PutScreenPixel(x+4,j,TCOL);
    PutScreenPixel(x+11,j,TCOL);
    PutScreenPixel(x+12,j,TCOL);
  }
}

void exits(char *s,int e=0)
{
   printf("\n\n");
   if(e==0) printf("Out of memory ");
   if(e==2) printf("File does not exist: ");
   if(e==3) printf("Can't open file: ");
   if(e==4) printf("Too many options ");
   if(e==999) printf("Crack Alert ");
   printf("%s !\n\n",s);
   exit(1);
}

FILE *fout = NULL;

void commands(char *fstr)
{
   if(f_d>=2&&ncom)
   {
      fout=fopen(FILEOUT,"at");
      fprintf(fout,">>> T=%u %s COMMANDS ",rtime,fstr);
      fprintf(fout,"ncom=%u ncom2=%u\n",ncom,ncom2);
      for(int i=0;i<ncom;i++)
      {
         fprintf(fout,"%u) ",i);
         if(i<ncom2) fprintf(fout," ");
         else fprintf(fout,"*");
         fprintf(fout,"[%4.4X]",command[i]);
         fprintf(fout," n=%i",comn[i]);
         fprintf(fout," x=%i",comx[i]);
         fprintf(fout," y=%i",comy[i]);
         fprintf(fout," k=%i",comk[i]);
         fprintf(fout," t=%i",comt[i]);
         fprintf(fout," d=%i",comd[i]);
         fprintf(fout,"\n");
      }
      fclose(fout);
   }
}

int badreg = 0;

int main(int argc0,char **argv0)
{
  int argc;
  char **argv;
  char *poa,*po;
  FILE *flist;
  int i,j,k,a[MNUM];
  dx = DDX*SIZE;
  dy = DDY*SIZE;
  char str[100],str2[100];
//  int sum=0;
  printf("\n\n%s\n",rwtitle);
  Rand=(time(NULL)%100)/100.0;
  argv = new char*[ARGCMAX];
  if(argv==NULL) exits("argv",0);
  j = 0;
  for(i=0;i<argc0;i++)
  {
     argv[j] = NULL;
     if(argv0[i][0]=='@')
     {
        poa = &argv0[i][1];
        flist = fopen(poa,"rt");
        if(flist==NULL) exits(poa,2);
        while(1)
        {
          fgets(str,100,flist);
          if(feof(flist)) break;
          if(*str=='\n') continue;
          argv[j] = new char[strlen(str)+1];
          if(argv[j]==NULL) exits("argv[]",0);
          po = str;
          k = 0;
          while(*po!='\n')
          {
            if(*po==' ') continue;
            if(*po=='\t') continue;
            if(*po==0) break;
            argv[j][k++] = *po;
            po++;
          }
          argv[j][k] = 0;
          if(++j>=ARGCMAX) exits("!",4);
        }
        fclose(flist);
     }
     else
     {
        argv[j] = new char[strlen(argv0[i])+1];
        if(argv[j]==NULL) exits("argv[]",0);
        strcpy(argv[j],argv0[i]);
        j++;
     }
     if(j>=ARGCMAX) exits("!",4);
  }
  argc = j;
  if(argc<2)
  {
   printf("\nUsage:\n\n");
   printf("   RW1_DUEL [options] ROBOT.RW1\n");
   printf("   RW1_DUEL [options] @list.txt\n");
   printf("   RW1_DUEL [options] ROBOT.RW1 ROBOT2.RW1\n");
   printf("   RW1_DUEL [options] ROBOT.RW1 ROBOT2.RW0\n");
   printf("   RW1_DUEL [options] ROBOT.RW0 ROBOT2.RW1\n");
   printf("   RW1_DUEL [options] ROBOT.RW0 ROBOT2.RW0\n");
   printf("\n   options:\n");
   printf("    -s0 : sound off                -r0 : 'WINSIMPLE' robot\n");
   printf("    -s1 : PC-Speaker (default)     -r1 : 'BUILT-IN 2' robot (default)\n");
   printf("                                   -r2 : 'MONSTER 7' robot\n");
   printf("    -f  : fast game\n");
   printf("    -f2 : super fast game without graphics\n");
   printf("    -d  : debug mode using RW1_DUEL.OUT file\n");
   printf("    -d2 : step by step debug mode (using RW1_DUEL.OUT file for output)\n");
   printf("    -d3 : substep debug mode (using RW1_DUEL.OUT file for output)\n");
   printf("    -nM : M - float [0..1] for map generating (i.e. -n0.2001)\n");
   printf("    -gT : fast running to T ticks\n");
   printf("    -gr : group mode\n");
   printf("    -dm : deathmatch mode\n");
   printf("    -mN : N - number of robots\n");
   printf("\n");
   return 0;
  }
  int i1,i2,i3,j1,j2,j3,nn,k2;
  int rn[MNUM];
  int re[MNUM];
  int rm[MNUM];
  for(i=0;i<MNUM;i++)
  {
     re[i] = ENERGYBEG;
     rm[i] = MISSLEBEG;
  }
  i = 0;
  for(k=1;k<argc;k++)
  {
     if(argv[k][0]=='-')
     {
        switch(argv[k][1])
        {
           case 'm':
              po = &argv[k][2];
              NUM = atoi(po);
              if(NUM>=MNUM) NUM=2;
              f_n = 1;
              break;
           case 'n':
              po = &(argv[k][2]);
              Rand=atof(po);
              break;
           case 'g':
              if(argv[k][2]=='r')
              {  f_gr = 1;
                 break;
              }
              po = &(argv[k][2]);
              f_g=atof(po);
              f_f=1;
              break;
           case 'i':
              f_i = 1;
              break;
           case 'd':
              if(argv[k][2]==0) f_d=1;
              if(argv[k][2]=='2') f_d=2;
              if(argv[k][2]=='3') f_d=3;
              if(argv[k][2]=='m') f_dm=1;
              break;
           case 'f':
              f_f=1;
              if(argv[k][2]=='2') f_f=2;
              break;
           case 's':
              f_s = -1;
              if(argv[k][2]=='0') f_s=0;
              if(argv[k][2]=='1') f_s=1;
              if(f_s==-1||strlen(argv[k])!=3) exits("Bad options");
              break;
           case 'r':
              f_r = -1;
              if(argv[k][2]=='0') f_r=0;
              if(argv[k][2]=='1') f_r=1;
              if(argv[k][2]=='2') f_r=2;
              if(argv[k][2]=='3') f_r=3;
              if(f_r==-1||strlen(argv[k])!=3) exits("Bad options");
              break;
           default:
              if(isdigit(argv[k][1]) && i>0)
              {
                 po = strchr(argv[k],'/');
                 if(po!=NULL)
                 {
                    po++;
                    rm[i-1] = atoi(po);
                 }
                 po = &argv[k][1];
                 re[i-1] = atoi(po);
              //   if(re[i-1]>ENERGYMAX) re[i-1]=ENERGYMAX
              }
              else exits("Bad options");
              break;
        }
     }
     else
     {
        FILE *f11 = fopen(argv[k],"rb");
        if(f11==NULL) exits(argv[k],2);
        fclose(f11);
        rn[i++] = k;
     }
  }
  int irob = i;
  if(irob>NUM&&irob<MNUM) NUM=irob;
  double zapRand = Rand;

  if(f_d)
  {  fout=fopen(FILEOUT,"wt");
     if(fout==NULL) exits(FILEOUT,3);
  }
  if(f_d>=2) fclose(fout);

  po = strrchr(argv[rn[0]],'.');
  int ok=0;
  if(!strcmp(po,".rw1")) ok=1;
  if(!strcmp(po,".RW1")) ok=1;
  if(!strcmp(po,".rw0")) ok=2;
  if(!strcmp(po,".RW0")) ok=2;
  if(!ok) exits("File extention is bad");
  for(i=0;i<MNUM;i++) r[i]=NULL;
  r[0]=new Robot(1,argv[rn[0]]);
  if(r[0]==NULL) exit(1);

  if(irob==1&&NUM==2)
  {
   unsigned char *robo;
   if(f_r==0) robo = robot0;
   if(f_r==1) robo = robot1;
   if(f_r==2) robo = robot2;
   if(f_r==3) robo = robot3;
   r[1] = new Robot(2);
   if(r[1]==NULL) r[0]->Error(0,"new Robot 2");
   r[1]->SetCode(rlen[f_r],robo);
   r[1]->SetVar(vlen[f_r]);
   r[1]->SetName(robotname[f_r]);
   if(f_r==3) r[1]->SetEquip(2,0,1,0);
   else r[1]->SetEquip(1,0,0,2);
   if(f_r==0) r[1]->SetColor(0xFF,0xD0,0x10);
   if(f_r==1) r[1]->SetColor(0x00,0x00,0x00);
   if(f_r==2) r[1]->SetColor(0x00,0x00,0x00);
   if(f_r==3) r[1]->SetColor(0x00,0x00,0x00);
   r[1]->SetImage(0);
   if(f_r==2)
   {
     char im2[64] = {
     0x0,  0x0,  0xC,  0xC,  0xC,  0xC,  0x0,  0x0,
     0x0,  0xC,  0x0,  0x0,  0x0,  0x0,  0xC,  0x0,
     0xC,  0x0,  0x0,  0xC,  0xC,  0x0,  0x0,  0xC,
     0xC,  0x0,  0xC,  0x0,  0x0,  0xC,  0x0,  0xC,
     0xC,  0x0,  0xC,  0x0,  0xC,  0x0,  0x0,  0xC,
     0xC,  0x0,  0x0,  0xC,  0x0,  0x0,  0xC,  0x0,
     0x0,  0xC,  0x0,  0x0,  0xC,  0xC,  0x0,  0x0,
     0x0,  0x0,  0xC,  0x0,  0x0,  0x0,  0x0,  0x0
     };
     r[1]->SetImage(1,(void*)im2);
   }
   if(f_r==3)
   {
     char im3[64] = {
     0x2,  0x2,  0x2,  0x0,  0x0,  0x2,  0x2,  0x2,
     0x2,  0x2,  0x0,  0x8,  0x8,  0x0,  0x2,  0x2,
     0x2,  0x0,  0x8,  0x8,  0x8,  0x8,  0x0,  0x2,
     0x2,  0x0,  0x8,  0x8,  0x8,  0x8,  0x0,  0x2,
     0x2,  0x0,  0x8,  0x0,  0x0,  0x8,  0x0,  0x2,
     0x2,  0x0,  0x8,  0x0,  0x0,  0x8,  0x0,  0x2,
     0x2,  0x0,  0x8,  0x8,  0x8,  0x8,  0x0,  0x2,
     0x2,  0x2,  0x0,  0x0,  0x0,  0x0,  0x2,  0x2
     };
     r[1]->SetImage(1,(void*)im3);
   }
  }

  if(irob==2&&NUM==2)
  {
     r[1] = new Robot(2,argv[rn[1]]);
     if(r[1]==NULL) exits("new Robot");
  }

  if(NUM>2)
  {
     for(i=1;i<irob;i++)
     {
         r[i]=new Robot(i+1,argv[rn[i]]);
         if(r[i]==NULL) exits("new Robot");
     }
     if(NUM>irob)
     {
         for(i=irob;i<NUM;i++)
         {
            r[i]=new Robot(i+1,argv[rn[irob-1]]);
            if(r[i]==NULL) exits("new Robot");
         }
     }
  }

 #if 0
  tempty.Save("empty");
  thole.Save("hole");
  tstone.Save("stone");
  tbox.Save("box");
  treactor.Save("reactor");
  tmissle0.Save("missle0");
  tmissle1.Save("missle1");
  tmissle2.Save("missle2");
  tmissle3.Save("missle3");
  tfire.Save("fire");
 #endif

  map = MakeMap(NUM,DDX,DDY);
  int x[MNUM],y[MNUM];
  if(f_f!=2)
  {
    dac = new Palette256;
    if(dac==NULL) exit(1);
    SetGamePalette();
    for(j=0;j<DDY;j++){
    for(i=0;i<DDX;i++){
        switch(map[i][j]>>4)
        {
           case 0: tempty.Draw(i<<4,j<<4); break;
           case 1: thole.Draw(i<<4,j<<4); break;
           case 2: tstone.Draw(i<<4,j<<4); break;
           case 3: tbox.Draw(i<<4,j<<4); break;
           case 4: treactor.Draw(i<<4,j<<4); break;
           case 6: tempty.Draw(i<<4,j<<4);
                   k = map[i][j]&0x0F;
                   x[k] = i;
                   y[k] = j;
                   break;
        }
    }}
    InitPov();
  }
  else
  {
    for(i=0;i<DDX;i++){
    for(j=0;j<DDY;j++){
        if((map[i][j]>>4)==6)
        {
          k = map[i][j]&0x0F;
          x[k] = i;
          y[k] = j;
        }
    }}
  }

  short col[MNUM],kk;
  for(k=0;k<NUM;k++)
  {
    rwstat[k].time = 0;
    rwstat[k].all_shoot = 0;
    rwstat[k].good_shoot = 0;
    rwstat[k].kill_shoot = 0;
    rwstat[k].min_missle = 100;
    rwstat[k].max_missle = 0;
    rwstat[k].fin_missle = 100;
    rwstat[k].min_energy = 100;
    rwstat[k].max_energy = 0;
    rwstat[k].fin_energy = 0;
    rwstat[k].boxes = 0;
    rwstat[k].damages = 0;
    rwstat[k].reason = 0;
    ms[k].n=-1;
    col[k]=31-k;
    if(f_f!=2) dac->SetPalette( 31-k,RGB( (r[k]->GetR())>>2,
                               (r[k]->GetG())>>2,
                               (r[k]->GetB())>>2
                              ));
    r[k]->SetLoc(x[k],y[k]);
    r[k]->SetMap(DDX,DDY,map);
    for(i=0;i<8;i++){
    for(j=0;j<8;j++){
      if(r[k]->fimage) kk=r[k]->GetI(i,j);
      else kk=col[k];
      if(kk==0) kk=192;
      if(f_f!=2) trobot[k].PutPixel(i+4,j+4,kk);
    }}
    if(r[k]->fimage) kk=col[k];
    else kk=192;

    if(f_f!=2)
    {
       for(i=0;i<10;i++)
       {
          trobot[k].PutPixel(3,i+3,kk);
          trobot[k].PutPixel(12,i+3,kk);
          trobot[k].PutPixel(i+3,3,kk);
          trobot[k].PutPixel(i+3,12,kk);
       }
       for(i=0;i<4;i++)
       {
         if(r[k]->GetE(i)==GUN)
         {
          for(j=0;j<2;j++)
          {
             switch(i)
             {
               case 1:
                 trobot[k].PutPixel(13+j,7,192);
                 trobot[k].PutPixel(13+j,8,192);
                 break;
               case 0:
                 trobot[k].PutPixel(7,2-j,192);
                 trobot[k].PutPixel(8,2-j,192);
                 break;
               case 3:
                 trobot[k].PutPixel(2-j,7,192);
                 trobot[k].PutPixel(2-j,8,192);
                 break;
               case 2:
                 trobot[k].PutPixel(7,13+j,192);
                 trobot[k].PutPixel(8,13+j,192);
                 break;
             }
          }
         }
         if(r[k]->GetE(i)==EYE)
         {
          for(j=0;j<2;j++)
          {
             switch(i)
             {
               case 1:
                 trobot[k].PutPixel(13,j+5,192);
                 trobot[k].PutPixel(13,j+7,11);
                 trobot[k].PutPixel(13,j+9,192);
                 break;
               case 0:
                 trobot[k].PutPixel(j+5,2,192);
                 trobot[k].PutPixel(j+7,2,11);
                 trobot[k].PutPixel(j+9,2,192);
                 break;
               case 3:
                 trobot[k].PutPixel(2,j+5,192);
                 trobot[k].PutPixel(2,j+7,11);
                 trobot[k].PutPixel(2,j+9,192);
                 break;
               case 2:
                 trobot[k].PutPixel(j+5,13,192);
                 trobot[k].PutPixel(j+7,13,11);
                 trobot[k].PutPixel(j+9,13,192);
                 break;
             }
          }
         }
       }
       switch(r[k]->angle)
       {
         case 2: trobot[k].Left();
                 break;
         case 3: trobot[k].Right();
         case 0: trobot[k].Right();
         case 1: break;
       }
       RShow(k);
    }
  }

  int maxgr = 0;
  char *pstr,last[100];
  for(i=0;i<NUM;i++)
  {
      k = 0;
      for(j=0;j<maxgr;j++)
      {
        if(!strcmp(gr[j].author,r[i]->GetAuthor())){k=1;break;}
      }
      if(!k)
      {
        maxgr++;
        strcpy(gr[j].author,r[i]->GetAuthor());
      }
      r[i]->E = re[i];
      r[i]->M = rm[i];
      r[i]->SetLast(last);
      r[i]->SetRar(r,NUM,f_gr);
      rstep[i] = -1;
  }

  if(f_i)
  {
    printf("\n");
    printf("* ver=1.99 open\n");
    printf("* num=%u\n",NUM);
    printf("* group=%u\n",maxgr);
    printf("* dx=%u\n",DDX);
    printf("* dy=%u\n",DDY);
    printf("* rand=%8.8lf\n",zapRand);
  }

  if(f_f!=2)
  {
    dac->SetHardPalette();
    DrawBar(0,160,320,40,134);
                    //0123456789012345678901234567890123456789
    DrawString(0,162,rwtitle,140,130);
    DrawString(0,172,"                               TIME    0",130,134);
    for(i=0;i<2;i++)
    {
      if(f_gr) strcpy(str,gr[i].author);
      else strcpy(str,r[i]->GetName());
      DrawString(56,181+i*10,str,143,134);
      DrawChar(48,181+i*10,':',130,134);
      if(f_gr) sprintf(str,"GROUP%u",i+1);
      else sprintf(str,"ROBOT%u",i+1);
      DrawString(0,181+i*10,str,col[i],134);
    }

  }

  rtime = 0;
  ok = 1;
  ncom = 0;
  int ibre = 2000;
  int mmi;
  int ii,jj;
  int edrobo;
  while(ok) // MAINLOOP
  {
     if(kbhit()&&getch()==27) break;
     sprintf(str,"%u",rtime++);
     if(f_f!=2) DrawString(320-strlen(str)*8,172,str,130,134);
     if(f_i&&rtime>=TIMEMAX) ok=0;
     if(f_g>0&&rtime>=f_g) f_g=f_f=0;
     if(f_g==0&&f_d==2&&rtime>1) if(getch()==27) break;
     if(f_dm&&(rtime%TIMEDM)==0)
     {
        for(j=0;j<_N_box;j++)
        {
            if((map[_X_box[j]][_Y_box[j]]>>4)==0)
            {   map[_X_box[j]][_Y_box[j]] = 0x30;
                if(f_f!=2) tbox.Draw(_X_box[j]<<SBIT,_Y_box[j]<<SBIT);
            }
        }
     }

     if(f_g==0&&f_d>=2)
     {  fout=fopen(FILEOUT,"at");
        if(fout==NULL) exits(FILEOUT,3);
     }

     if(f_gr)
     {
       for(k=0;k<maxgr;k++)
       {
         gr[k].emax = 0;
         gr[k].mmax = 0;
         gr[k].num = 0;
         for(ii=0;ii<NUM;ii++)
         {
            if(!strcmp(r[ii]->GetAuthor(),gr[k].author))
            {
               if(r[ii]->E <= 0) continue;
               gr[k].num++;
               if(r[ii]->E > gr[k].emax) gr[k].emax=r[ii]->E;
               if(r[ii]->M > gr[k].mmax) gr[k].mmax=r[ii]->M;
            }
         }

         if(k<2&&f_f!=2)
         {
           sprintf(str,"%s [%u]",gr[k].author,gr[k].num);
           DrawString(56,181+k*10,str,143,134);
           DrawChar(STARTE,181+k*10,':',130,134);
           for(ii=0;ii<13;ii++)
           {
              if(ii<gr[k].emax) jj=0x7F;
              else jj=0x20;
              DrawChar(STARTE+(ii+1)*8,181+k*10,jj,143,134);
           }
           sprintf(str,"%u",gr[k].mmax);
           TraceWait();
           DrawString(288,181+10*k,"    ",143,134);
           DrawString(320-strlen(str)*8,181+10*k,str,143,134);
         }
       }
     }

     for(k=0;k<NUM;k++)
     {
        i = r[k]->GetX();
        j = r[k]->GetY();
        a[k] = r[k]->angle;

        if(r[k]->E>0)
        {

           #ifdef FULLDEB
           if(f_g==0&&f_d>0)
           {
               fprintf(fout,"%4.4u <%s>\t",rtime,r[k]->GetName());
               fprintf(fout,"[%2.2X] ",r[k]->GetCode(r[k]->pc));
               fprintf(fout,"PC=%u ",r[k]->pc);
               fprintf(fout,"SP=%i ",VARMAX-1-r[k]->sp);
               fprintf(fout,">>> ");
           }
           #endif

           pstr = r[k]->Step(fout);

           #ifdef FULLDEB
           if(f_g==0&&f_d>0)
           {
               fprintf(fout,"T=%i ",r[k]->T);
               fprintf(fout,"X=%i ",r[k]->X);
               fprintf(fout,"Y=%i ",r[k]->Y);
               fprintf(fout,"E=%i ",r[k]->E);
               fprintf(fout,"M=%i ",r[k]->M);
               fprintf(fout,"N=%i ",r[k]->N);
               fprintf(fout,"D=%i ",r[k]->D);
               fprintf(fout,"K=%i ",r[k]->K);
               fprintf(fout,"R=%i ",r[k]->R);
               fprintf(fout,"comm=%4.4X\n",r[k]->command);

               #ifdef MAPDRAW
               fprintf(fout,"-------map-------\n");
               for(int jm=0;jm<10;jm++){
               for(int im=0;im<20;im++){
                   fprintf(fout," %2.2X",map[im][jm]);
               }
               fprintf(fout,"\n");
               }
               fprintf(fout,"-------map-------\n");
               #endif
           }
           #endif

           rwstat[k].time = rtime; // r[k]->T;

           if(r[k]->E<=0)
           {
              if((r[k]->command>>8)==5)
              {
                rwstat[k].reason=7;
                sprintf(str,"Robot %u died by send !",k+1);
              }
              else
              {
                rwstat[k].reason=4;
                sprintf(str,"Robot %u died by radar !",k+1);
              }
              if(f_f!=2) Message(str);
              RShow(k);
           }

           if(r[k]->halt)
           {
              r[k]->E=0;
              rwstat[k].reason=6;
              sprintf(str,"Robot %u : HALT !",k+1);
              if(f_f!=2) Message(str);
              RShow(k);
           }
        }
        if(rwstat[k].fin_missle < r[k]->M) rwstat[k].boxes++;
        if(rwstat[k].min_missle > r[k]->M) rwstat[k].min_missle=r[k]->M;
        if(rwstat[k].max_missle < r[k]->M) rwstat[k].max_missle=r[k]->M;
        rwstat[k].fin_missle = r[k]->M;
        if(rwstat[k].min_energy > r[k]->E) rwstat[k].min_energy=r[k]->E;
        if(rwstat[k].max_energy < r[k]->E) rwstat[k].max_energy=r[k]->E;
        rwstat[k].fin_energy = r[k]->E;

        if(k<2&&f_f!=2&&!f_gr)
        {
          DrawChar(STARTE,181+k*10,':',130,134);
          for(ii=0;ii<13;ii++)
          {
             if(ii<r[k]->E) jj=0x7F;
             else jj=0x20;
             DrawChar(STARTE+(ii+1)*8,181+k*10,jj,143,134);
          }
          sprintf(str,"%u",r[k]->M);
          TraceWait();
          DrawString(288,181+10*k,"    ",143,134);
          DrawString(320-strlen(str)*8,181+10*k,str,143,134);
        }

        if(r[k]->GetX()<0||r[k]->E<=0)
        {
           if(r[k]->GetX()>0&&r[k]->GetY()>0)
              map[r[k]->GetX()][r[k]->GetY()] = 0;
           r[k]->SetX(-1);
           r[k]->SetY(-1);
           continue;
        }
        if(pstr!=NULL&&f_f!=2)
        {
           Message(pstr,k);
        }
        if(r[k]->command!=0)
        {
           command[ncom] = 0x1000 + (r[k]->command&0xF00) + k;
           comx[ncom] = r[k]->GetX();
           comy[ncom] = r[k]->GetY();
           comt[ncom] = 0;
           comd[ncom] = 0;
           comn[ncom] = 0;
           comk[ncom++] = r[k]->command&0xFF;
        }
        if(r[k]->angle!=a[k])
        {
           i1 = r[k]->angle - a[k];
           if(i1!=1&&i1!=-1)
           {
              if(i1<0) i1=1;
              else i1=-1;
           }
           command[ncom] = 0x200 + k;
           comx[ncom++] = i1;
        }
        if(i!=r[k]->GetX()||j!=r[k]->GetY())
        {
           command[ncom] = 0x300 + k;
           comx[ncom] = i;
           comy[ncom++] = j;
        }
     }

     #define R1(i) ((i)/(double)NPOV)
     #define R2(i) ((NPOV-(i))/(double)NPOV)
     #define RR(i1,i2,j) ((i1)*R2(j)+(i2)*R1(j))

     int ibreak = 0;

     for(j=0;j<=NPOV;j++)
     {
        if(ncom==0) break;
        ncom2 = ncom;
        for(i=0;i<ncom2;i++)
        {  int ix_,iy_;
           switch(command[i]>>8)
           {
             case 0x00:
                  break;
             case 0x01:
                  nn = command[i]&0xFF;
                  switch(nn)
                  {
                    case 1: Fire1(comx[i],comy[i],4-comk[i],comn[i]); break;
                    case 2: Fire2(comx[i],comy[i],4-comk[i],comn[i]); break;
                  }
                  Sound(FIREF-j,FIRES*nn);
                  if(comk[i]--==0)
                  {
                     command[i]=0;
                  }
                  break;
             case 0x02:
                  k = command[i]&0xFF;
                  if(r[k]->E<=0) break;
                  if(comx[i]>0) LeftPov(k,j);
                  else RightPov(k,j);
                  Sound(1,DELA/ncom2);
                  break;
             case 0x03:
                  k = command[i]&0xFF;
                  if(r[k]->E<=0) break;
                  i1 = comx[i]<<SBIT;
                  j1 = comy[i]<<SBIT;
                  i2 = (r[k]->GetX())<<SBIT;
                  j2 = (r[k]->GetY())<<SBIT;
                  TraceWait();
                  if(f_f!=2)
                  {
                     tempty.Draw(i1,j1);
                     tempty.Draw(i2,j2);
                  }
                  ix_ = RR(i1,i2,j);
                  iy_ = RR(j1,j2,j);
                  switch(rstep[k])
                  {
                    case 0:
                         htrace(i1+1,j1,i1+1);
                         if(a[k]==0) htrace(i1+3,j1,i1+11);
                         break;
                    case 1:
                         vtrace(i1,j1+13,j1+15);
                         if(a[k]==1) vtrace(i1,j1+3,j1+11);
                         break;
                    case 2:
                         htrace(i1+13,j1,i1+15);
                         if(a[k]==2) htrace(i1+3,j1,i1+11);
                         break;
                    case 3:
                         vtrace(i1,j1+1,j1+1);
                         if(a[k]==3) vtrace(i1,j1+3,j1+11);
                         break;
                  }
                  switch(a[k])
                  {
                    case 0: htrace(i1+13,j1,ix_+3); break;
                    case 1: vtrace(i1,iy_+13,j1+1); break;
                    case 2: htrace(ix_+13,j1,i1+1); break;
                    case 3: vtrace(i1,j1+13,iy_+3); break;
                  }
                  if(f_f!=2) trobot[k].Draw(ix_,iy_);
                  if(j==NPOV)
                  {
                     rstep[k]=a[k];
                  }
                  Sound(20+k*10,DELA/ncom2);
                  break;
             case 0x11:
                  if(comd[i]) break;
                  k = command[i]&0xFF;
                  comt[i]++;
                  i2 = comx[i];
                  j2 = comy[i];
                  if(i2==r[k]->GetX()&&j2==r[k]->GetY())
                     rwstat[k].all_shoot++;
                  switch(comk[i])
                  {
                     case 0:
                          if(++comx[i]>=DDX) comd[i]=1;
                          else if(f_f!=2) tmissle0.Draw(comx[i]<<SBIT,comy[i]<<SBIT);
                          break;
                     case 1:
                          if(--comy[i]<0) comd[i]=1;
                          else if(f_f!=2) tmissle1.Draw(comx[i]<<SBIT,comy[i]<<SBIT);
                          break;
                     case 2:
                          if(--comx[i]<0) comd[i]=1;
                          else if(f_f!=2) tmissle2.Draw(comx[i]<<SBIT,comy[i]<<SBIT);
                          break;
                     case 3:
                          if(++comy[i]>=DDY) comd[i]=1;
                          else if(f_f!=2) tmissle3.Draw(comx[i]<<SBIT,comy[i]<<SBIT);
                          break;
                  }
                  if(comd[i]) comn[i]=10;
                  else
                  {
                     comn[i]=map[comx[i]][comy[i]]>>4;
                  }
                  if((map[i2][j2]>>4)==5)
                  {
                     map[i2][j2] = 0;
                     if(f_f!=2) tempty.Draw(i2<<SBIT,j2<<SBIT,5);
                  }
                  if(comd[i]) break;
                  i1 = comx[i]<<SBIT;
                  j1 = comy[i]<<SBIT;
                  switch(comn[i])
                  {
                     case 0: map[comx[i]][comy[i]] = 0x50|k;
                             break;
                     case 1: if(f_f!=2) thole.Draw(i1,j1);
                             break;
                     case 2: k2 = map[comx[i]][comy[i]]&0x0F;
                             if(f_f!=2) tfire.Draw(i1,j1);
                             Sound(FIREF,FIRES);
                             if(--k2>0)
                             {  if(f_f!=2)
                                {  tstone.Draw(i1,j1);
                                   DrawChar(i1+4,j1+4,k2+'0',192,210);
                                }
                                map[comx[i]][comy[i]] = 0x20+k2;
                             }
                             else
                             {  if(f_f!=2) tempty.Draw(i1,j1);
                                map[comx[i]][comy[i]] = 0;
                             }
                             comd[i] = 1;
                             break;
                     case 3: command[ncom]=0x101;
                             comk[ncom] = NPOV;
                             comn[ncom] = k;
                             comd[ncom] = 0;
                             comx[ncom] = comx[i];
                             comy[ncom++] = comy[i];
                             Sound(FIREF,FIRES);
                             comd[i] = 1;
                             break;
                     case 4: command[ncom]=0x102;
                             comk[ncom] = NPOV;
                             comn[ncom] = k;
                             comd[ncom] = 0;
                             comx[ncom] = comx[i];
                             comy[ncom++] = comy[i];
                             Sound(FIREF,FIRES);
                             comd[i] = 1;
                             break;
                     case 5: for(nn=0;nn<ncom2;nn++)
                             {
                               if(  nn != i &&
                                   (command[nn]>>8)==0x11 &&
                                    comx[nn]==comx[i] &&
                                    comy[nn]==comy[i] )
                               {
                                  comd[nn] = 1;
                                  comn[nn] = 5;
                               }
                             }
                             if(f_f!=2)
                             {  tfire.Draw(i1,j1);
                                Sound(FIREF,FIRES);
                                tempty.Draw(i1,j1);
                             }
                             map[comx[i]][comy[i]] = 0;
                             comd[i] = 1;
                             break;
                     case 6: for(nn=0;nn<NUM;nn++)
                             {
                              if(r[nn]->GetX()==comx[i]&&r[nn]->GetY()==comy[i])
                              {
                                 RoboDead(nn,1);
                                 rwstat[k].good_shoot++;
                                 if(r[nn]->E<=0)
                                 {  rwstat[k].kill_shoot++;
                                    rwstat[nn].reason = 2;
                                 }
                                 rwstat[nn].damages++;
                                 break;
                              }
                             }
                             if(f_f!=2) tfire.Draw(i1,j1);
                             Sound(FIREF,FIRES);
                             RShow(nn);
                             comd[i] = 1;
                             break;
                     default:
                             if(f_f!=2) CloseGraph();
                             printf("\nFatal Error: 1001\n\n");
                             printf("\tcomn=%i x=%i y=%i\n",
                                    comn[i],comx[i],comy[i]);
                             exit(1);
                             break;
                  }
                  Sound(1000+100*k-comt[i]*10,DELA/ncom2);
                  break;
             case 0x12:
                  k = command[i]&0xFF;
                  if(f_f!=2)
                  {
                     i1 = comx[i]<<SBIT;
                     j1 = comy[i]<<SBIT;
                     switch(comk[i])
                     {
                       case 0:
                         PutScreenPixel(i1+13,j1+7,15);
                         PutScreenPixel(i1+13,j1+8,15);
                         break;
                       case 1:
                         PutScreenPixel(i1+7,j1+2,15);
                         PutScreenPixel(i1+8,j1+2,15);
                         break;
                       case 2:
                         PutScreenPixel(i1+2,j1+7,15);
                         PutScreenPixel(i1+2,j1+8,15);
                         break;
                       case 3:
                         PutScreenPixel(i1+7,j1+13,15);
                         PutScreenPixel(i1+8,j1+13,15);
                         break;
                     }
                  }
                  command[i] = 0;
                  break;
             case 0x13:
                  k = command[i]&0xFF;
                  r[k]->N = ms[k].n;
                  i2 = ms[k].x;
                  i3 = ms[k].y;
                  if(f_gr && r[k]->N==6)
                  {
                     for(k2=0;k2<NUM;k2++)
                     {
                         if( r[k]->GetX()==i2 &&
                             r[k]->GetY()==i3 &&
                             !strcmp(r[k]->GetAuthor(),
                                     r[k2]->GetAuthor())
                           ) r[k]->N = 7;
                     }
                  }
                  k2 = r[k]->angle;
                  if(k2<0) k2+=4;
                  if(k2>3) k2-=4;
                  switch(k2)
                  {
                    case 0: r[k]->X=i3-r[k]->GetY();
                            r[k]->Y=i2-r[k]->GetX();
                            break;
                    case 1: r[k]->X=i2-r[k]->GetX();
                            r[k]->Y=r[k]->GetY()-i3;
                            break;
                    case 2: r[k]->X=r[k]->GetY()-i3;
                            r[k]->Y=r[k]->GetX()-i2;
                            break;
                    case 3: r[k]->X=r[k]->GetX()-i2;
                            r[k]->Y=i3-r[k]->GetY();
                            break;
                  }
                  command[i] = 0;
                  break;
             case 0x14:
                  k = command[i]&0xFF;
                  i2 = r[k]->GetX();
                  i3 = r[k]->GetY();
                  map[i2][i3] = 0;
                  i2 <<= SBIT;
                  i3 <<= SBIT;
                  if(f_f!=2) tempty.Draw(i2,i3);
                  r[k]->E = 0;
                  r[k]->N = 0;
                  rwstat[k].reason = 1;
                  sprintf(str,"Robot %u has gone into the HOLE !",k+1);
                  if(f_f!=2) Message(str);
                  command[i] = 0;
                  break;
             case 0x15:
                  k = command[i]&0xFF;
             //     if(f_gr)
                  {
                   if(comk[i]==0)
                   {
                      for(i1=0;i1<NUM;i1++)
                      {
                             if(i1==k) continue;
                             r[i1] -> Send(r[k]->send,k+1,
                                           r[k]->T,comx[i],comy[i]);
                             if(f_g==0&&f_d>0)
                             {
                                fprintf(fout,"T=%i\tROBOT%u '%s'\tSEND %i %i (%i,%i)\n",
                                  r[k]->T,k+1,r[k]->GetName(),
                                  r[k]->send,i1+1,
                                  comx[i],comy[i]);
                             }
                      }
                   }
                   if(comk[i]>0&&comk[i]<=NUM&&comk[i]!=k+1)
                   {
                      r[comk[i]-1] -> Send(r[k]->send,k+1,
                                           r[k]->T,comx[i],comy[i]);
                             if(f_g==0&&f_d>0)
                             {
                                fprintf(fout,"T=%i\tROBOT%u '%s'\tSEND %i %i (%i,%i)\n",
                                  r[k]->T,k+1,r[k]->GetName(),
                                  r[k]->send,comk[i],
                                  comx[i],comy[i]);
                             }
                   }
                  }
                  command[i] = 0;
                  break;
           }
        }

        if(f_g==0&&f_d==3)
        {  if(getch()==27)
           {  ibreak = 1;
              break;
           }
        }

     }
     if(ibreak) break;
     if(ncom==0) delayf(DELA);

     k = 0;
     for(i=0;i<ncom;i++)
     {
        j = command[i]>>8;
         if(j==0x11||j==0x01)
        {
           i1 = command[i]&0xFF;
           command[k] = command[i];
           if(j==0x11)
           {
             ms[i1].n = comn[i];
             ms[i1].x = comx[i];
             ms[i1].y = comy[i];
           }
           comn[k] = comn[i];
           comx[k] = comx[i];
           comy[k] = comy[i];
           comk[k] = comk[i];
           comt[k] = comt[i];
           comd[k] = comd[i];
           if(comd[i]==0) k++;
        }
     }
     ncom = k;

     if(ibre>1000)
     {
       mmi = 0;
       if(f_gr)
       {
        for(k=0;k<maxgr;k++)
        {
           if(gr[k].num>0)
           {
              mmi++;
              edrobo = k;
           }
        }
       }
       else
       {
        for(k=0;k<NUM;k++)
        {
           if(r[k]->E>0)
           {
              mmi++;
              edrobo = k;
           }
        }
       }
       if(mmi<=1) ibre=1;
     }
     else
     {
        ibre--;
        if(ibre<=0&&f_i) ok=0;
     }

     if(f_g==0&&f_d>=2)
     {  fclose(fout);
        delay(250);
     }
  }

  if(f_f!=2) CloseGraph();

  int wins = -1;
  if(f_gr)
  {
    if(mmi==1)
    {
       sprintf(str,"\n>>>%s WINS !\n\n",gr[edrobo].author);
       printf(str);
    }
  }
  else
  {
    if(mmi==1&&rwstat[edrobo].kill_shoot>0)
    {
       wins = edrobo+1;
       sprintf(str,"\n> ROBOT %u '%s' WINS !\n\n",wins,r[edrobo]->GetName());
       printf(str);
    }
  }
  if(f_f!=2) delete dac;

  for(k=0;k<NUM;k++)
  { if(f_i)
    {
     printf("> robot %u\n",k+1);
     printf("> name='%s'\n",r[k]->GetName());
     printf("> author='%s'\n",r[k]->GetAuthor());
     printf("> lcod=%u\n",r[k]->GetLCod());
     printf("> lvar=%u\n",r[k]->GetLVar());
     printf("> color=#%2.2X%2.2X%2.2X\n",
               r[k]->GetR(),r[k]->GetG(),r[k]->GetB());
     printf("> equip=%u%u%u%u\n",
               r[k]->GetE(0),r[k]->GetE(1),r[k]->GetE(2),r[k]->GetE(3));
     printf("> time=%u\n",rwstat[k].time);
     printf("> all_shoot=%u\n",rwstat[k].all_shoot);
     printf("> good_shoot=%u\n",rwstat[k].good_shoot);
     printf("> kill_shoot=%u\n",rwstat[k].kill_shoot);
     printf("> min_missle=%u\n",rwstat[k].min_missle);
     printf("> max_missle=%u\n",rwstat[k].max_missle);
     printf("> fin_missle=%u\n",rwstat[k].fin_missle);
     printf("> min_energy=%u\n",rwstat[k].min_energy);
     printf("> max_energy=%u\n",rwstat[k].max_energy);
     printf("> fin_energy=%u\n",rwstat[k].fin_energy);
     printf("> boxes=%u\n",rwstat[k].boxes);
     printf("> damages=%u\n",rwstat[k].damages);
     printf("> reason=%u\n",rwstat[k].reason);
     printf("\n");
    }
    if(r[k]!=NULL) delete r[k];
  }

  if(f_d==1) fclose(fout);

  for(i=0;i<ARGCMAX;i++)
  {
    if(i<argc) delete argv[i];
  }
  delete argv;

  return 0;
}
