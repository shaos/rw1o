// MY_VGA.HPP       Shabarshin A.A
// 320x200 256 colors.  12.03.1997
// for WATCOM           28.10.1997
// +DrawChar()          17.01.1998
// for LINUX (MY_VGAX)  27.10.2001 - 10.11.2001
// for SDL (MY_VGAS)    25.06.2009 - 06.07.2009

#ifndef __MY_VGA_H
#define __MY_VGA_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL/SDL.h>
#ifndef NO_FONT
#include "font5x9.h"
//#include "alt_8x16.h"
#endif

#define MAX_CVGA 255
#ifndef BYTE
#define BYTE unsigned char
#endif
#define VADR(x,y) ((x)+(y)*dX)

int dX = 0;
int dY = 0;
int fVGA = 0;

BYTE *VideoMem = NULL;

struct RGB
{  BYTE r,g,b;
   RGB(short r1,short g1,short b1){r=r1;g=g1;b=b1;};
   RGB(){r=MAX_CVGA;g=MAX_CVGA;b=MAX_CVGA;};
};

short _mode_;
BYTE keyPressed[128];
int mouseEvents[9]; // x,y,dx,dy,left,middle,right,up,dn
SDL_Surface* _screen_;

#define DEFAULT_MODE 0

#define SVGA256_320x200   1
#define SVGA256_640x400   2
#define SVGA256_640x480   3
#define SVGA256_800x600   4
#define SVGA256_1024x768  5

void SetVideoMode(short mode)
{   _mode_ = mode;
    if ( SDL_Init(SDL_INIT_VIDEO) < 0 ) 
    {
        printf("no SDL...\n");
        exit(-1);
    }
    switch(mode)
    {
/* androned: */
/*      case 0: dX=480; dY=295; break; */
/*      case 0: dX=320; dY=455; break; */
      case 0: dX=800; dY=600; break;
      case 1: dX=320; dY=200; break;
      case 2: dX=640; dY=400; break;
      case 3: dX=640; dY=480; break;
      case 5: dX=1024; dY=768; break;
    } 
    if(!dX||!dY)
    {
      SDL_Quit();
      exit(-3);
    }
#ifdef FULLSCREEN
    _screen_ = SDL_SetVideoMode(dX,dY,8,SDL_SWSURFACE|SDL_FULLSCREEN|SDL_HWPALETTE);
#else
    _screen_ = SDL_SetVideoMode(dX,dY,8,SDL_SWSURFACE);
#endif
    if(_screen_==NULL)
    {
      SDL_Quit();
      printf("SDL: no surface...\n");
      exit(-2);
    }
    printf("SDL mode %i\n",mode);
    SDL_ShowCursor(SDL_DISABLE);
}

short InitGraph(short mode=DEFAULT_MODE)
{   if(fVGA) return 0;
    fVGA=1;
    SetVideoMode(mode);
    VideoMem = new BYTE[dX*dY];
    memset(keyPressed,0,128);
    return 1;
}

inline void CloseGraph(void)
{
    delete VideoMem;
    SDL_Quit();
}

inline void PutScreenPixel(int x,int y,int c)
{
    if(x<0||x>=dX||y<0||y>=dY) return;
    VideoMem[VADR(x,y)]=c;
}

inline short GetScreenPixel(short x,short y)
{
    return VideoMem[VADR(x,y)];
}

short DrawChar(short x0,short y0,short ch,short cs,short cb=-1)
{
//    printf("DrawChar(%i,%i,%c,0x%2.2X,0x%2.2X)\n",x0,y0,ch,cs,cb);
    #ifdef NO_FONT
    printf("%c",ch);
    return 0;
    #else
//    if(_mode_!=SVGA256_320x200) return 0;
    if(ch<32||ch>127) return 0;
    short i,j,r;
    unsigned char *vptr0 = VideoMem + VADR(x0,y0);
    unsigned char *vptr;
    for(j=0;j<9;j++)
    {  r = font5x9[ch-32][j];
       vptr = vptr0 + dX*j;
       if(cb>=0) *vptr++ = cb;
       else vptr++;
       for(i=0;i<5;i++)
       {  if(r&1) *vptr=cs;
          else if(cb>=0) *vptr=cb;
          r>>=1;
          vptr++;
       }
       if(cb>=0)
       {
         *vptr++ = cb;
         *vptr++ = cb;
       }
       else vptr+=2;
    }
    return 1;
    #endif
}

short DrawString(short x0,short y0,char *s,short cs,short cb)
{
//    if(_mode_!=SVGA256_320x200) return 0;
    int i,x,k=strlen(s);
    for(i=0;i<k;i++)
    {  x = x0+i*8;
       if(x>=dX) break;
       DrawChar(x,y0,s[i],cs,cb);
    }
    return 1;
}

short DrawCharAlt (short x0,short y0,short ch,short cs,
                   short cb=-1,short ste=0,unsigned char *vm=NULL)
{
//    printf("DrawCharAlt(%i,%i,%c,0x%2.2X,0x%2.2X,%i,0x%8.8X)\n",x0,y0,ch,cs,cb,ste,(int)vm);
    #ifdef NO_FONT
    printf("%c",ch);
    return 0;
    #else
    if(!ste) ste=dX;
    if(vm==NULL) vm=VideoMem;
    auto short i,j,r;
    unsigned char *vptr0 = &vm[x0+ste*y0];
    auto unsigned char *vptr;
    for(j=0;j<16;j++)
    {  r = font[ch][j];
       vptr = &vptr0[ste*j];
       if(cb>=0) *vptr++=cb;
       else vptr++;
       for(i=0;i<8;i++)
       {  if(r&0x80) *vptr=cs;
          else if(cb>=0)
               *vptr = cb;
          r<<=1;
          vptr++;
       }
       if(cb>=0) *vptr++=cb;
       else vptr++;
    }
    return 1;
    #endif
}

short DrawBar(short x0,short y0,short dx,short dy,short c)
{
    short i,j;
//    if(_mode_!=SVGA256_320x200) return 0;    
    if(x0<0)
    {  dx=dx+x0;
       x0=0;
    }
    if(y0<0)
    {  dy=dy+y0;
       y0=0;
    }
    if((x0+dx)>=dX) dx=dX-x0;
    if((y0+dy)>=dY) dy=dY-y0;
    long ab=y0*dX+x0,at;
    for(j=0;j<dy;j++,ab+=dX){ at=ab;
    for(i=0;i<dx;i++,at++){
        VideoMem[at] = c;
    }}
    return c;
}

short HLine(short x0,short y0,short dx,short c)
{
//    if(_mode_!=SVGA256_320x200) return 0;
    if(x0<0)
    {  dx=dx+x0;
       x0=0;
    }
    short i;
    long a = y0*dX + x0;
    for(i=x0;i<x0+dx;i++,a++)
    {   if(i>=dX) break;
        VideoMem[a] = c;
    }
    return c;
}

short VLine(short x0,short y0,short dy,short c)
{
//    if(_mode_!=SVGA256_320x200) return 0;
    if(y0<0)
    {  dy=dy+y0;
       y0=0;
    }
    short j;
    long a = y0*dX + x0;
    for(j=y0;j<y0+dy;j++,a+=dX)
    {   if(j>=dY) break;
        VideoMem[a] = c;
    }
    return c;
}

struct Palette256
{
     SDL_Color pal[256];
     Palette256(){InitGraph();};
     Palette256(char *p)
     {  InitGraph();
        LoadPalette(p);
        SetHardPalette();
     };
     short LoadPalette(char* s);
     short SavePalette(char* s);
    ~Palette256(){;};
     void SetPalette(short i,RGB col)
     {  
        pal[i].r=col.r;
        pal[i].g=col.g;
        pal[i].b=col.b;
     };
     void SetHardPalette(void)
     {  
       SDL_SetColors(_screen_,pal,0,256);
     };
     RGB GetPalette(short i)
     {  RGB col;
        col.r=pal[i].r;
        col.g=pal[i].g;
        col.b=pal[i].b;
        return col;
     };
     void GetHardPalette(void)
     {
     };
};

short Palette256::LoadPalette(char* s)
{  int b,g,r,i;
   char ss[100];
   strcpy(ss,s);
   strcat(ss,".pal");
   FILE *f=fopen(ss,"rb");
   if(f==NULL) return 0;
   for(i=0;i<256;i++)
   {
       r=fgetc(f)<<2;
       g=fgetc(f)<<2;
       b=fgetc(f)<<2;
       SetPalette(i,RGB(r,g,b));
   }
   fclose(f);
   return 1;
}

short Palette256::SavePalette(char* s)
{  int i;
   RGB p;
   char ss[100];
   strcpy(ss,s);
   strcat(ss,".pal");
   FILE *f=fopen(ss,"wb");
   if(f==NULL) return 0;
   for(i=0;i<256;i++)
   {
       p=GetPalette(i);
       fputc(p.r>>2,f);
       fputc(p.g>>2,f);
       fputc(p.b>>2,f);
   }
   fclose(f);
   return 1;
}

void TraceWait(void)
{
 static int n = 0;
// static clock_t t = clock();
 if ( SDL_MUSTLOCK(_screen_) )
   if ( SDL_LockSurface(_screen_) < 0 )
        return;
 Uint8 *bits = (Uint8 *)_screen_->pixels;
 for(int j=0;j<dY;j++) 
      memcpy(&bits[_screen_->pitch*j],&VideoMem[dX*j],dX);
 if ( SDL_MUSTLOCK(_screen_) )
      SDL_UnlockSurface(_screen_);
 SDL_UpdateRect(_screen_,0,0,dX,dY);
 n++;
 memset(mouseEvents,0,sizeof(int)*9);
 SDL_Event event;
 while(SDL_PollEvent(&event))  
 {
  int key = event.key.keysym.scancode;
  SDLKey sym = event.key.keysym.sym;
  switch(event.type)
  {
   case SDL_KEYDOWN:
#ifdef linux
        key -= 8;
#endif 
        if(key>0 && key<128) keyPressed[key]=1;
//        if(key==15){n=1;t=clock();} // Tab to clean counters
        printf("KEY %i '%s'\n",key,SDL_GetKeyName(sym));
        break;
        
   case SDL_KEYUP:
#ifdef linux
        key -= 8;
#endif
        if(key>0 && key<128) keyPressed[key]=0;
//        printf("FPS=%i\n",(int)((double)n*CLOCKS_PER_SEC/(clock()-t)));
        break;
        
   case SDL_MOUSEBUTTONDOWN:
//        printf("MOUSE %i %i:%i\n",event.button.button,event.button.x,event.button.y);
        mouseEvents[3+event.button.button]++;
        mouseEvents[0] = event.button.x;
        mouseEvents[1] = event.button.y;
        break;
        
   case SDL_MOUSEMOTION:
//        printf("MOUSE ? %i:%i %i:%i\n",event.motion.x,event.motion.y,event.motion.xrel,event.motion.yrel);
        mouseEvents[0] = event.motion.x;
        mouseEvents[1] = event.motion.y;
        mouseEvents[2] += event.motion.xrel;
        mouseEvents[3] += event.motion.yrel;
        break;
  }
 }
// SDL_Delay(10);
};

unsigned long GetMSec(void)
{
  return SDL_GetTicks();
}

#endif
