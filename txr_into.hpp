/*
 Robot Warfare 1 Open Game
 =========================

 Copyright (c) 1998-2001 A.A.Shabarshin <me@shaos.net>

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom
 the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

// TXR_INTO.HPP - Shabarshin A.A.  12.11.1998

#ifndef _TXR_INTO_HPP
#include "txr_lib.h"

struct Texture
{  int col;
   int row;
   int size;
   unsigned char *image;
   Texture();
   Texture(int c,int r);
   Texture(int c,int r,unsigned char *img);
   int Save(char *n);
   int GetPixel(int i,int j){return image[j*col+i];};
   void PutPixel(int i,int j,int c){image[j*col+i]=c;};
   void Left(void);
   void Right(void);
   void Draw(int x,int y,int d=-1);
};

Texture::Texture()
{
   col = row = 16;
   long sz = col*row;
   if(sz>32766) exit(1);
   size = sz;
   image = new unsigned char[size];
   if(image==NULL) exit(1);
   for(int i=0;i<size;i++) image[i]=0;
}


Texture::Texture(int c,int r)
{
   long sz = c*r;
   if(sz>32766) exit(1);
   size = sz;
   col = c;
   row = r;
   image = new unsigned char[size];
   if(image==NULL) exit(1);
   for(int i=0;i<size;i++) image[i]=0;
}

Texture::Texture(int c,int r,unsigned char *img)
{
   col = c;
   row = r;
   size = col*row;
   image = img;
}

int Texture::Save(char *n)
{
   int i,j;
   char s[100];
   strcpy(s,n);
   strcat(s,".txr");
   FILE *f = fopen(s,"wb");
   if(f==NULL) return 0;
   fputc(col,f);
   fputc(row,f);
   for(j=0;j<row;j++){
   for(i=0;i<col;i++){
      fputc(GetPixel(i,j),f);
   }}
   fclose(f);
   return 1;
}

void Texture::Right(void)
{
   int b,i,j;
   for(i=0;i<col>>1;i++){
   for(j=0;j<row>>1;j++){
       b = GetPixel(i,j);
       PutPixel(i,j,GetPixel(j,col-i-1));
       PutPixel(j,col-i-1,GetPixel(col-i-1,col-j-1));
       PutPixel(col-i-1,col-j-1,GetPixel(col-j-1,i));
       PutPixel(col-j-1,i,b);
   }}
}

void Texture::Left(void)
{
   int b,i,j;
   for(i=0;i<col>>1;i++){
   for(j=0;j<row>>1;j++){
       b = GetPixel(i,j);
       PutPixel(i,j,GetPixel(col-j-1,i));
       PutPixel(col-j-1,i,GetPixel(col-i-1,col-j-1));
       PutPixel(col-i-1,col-j-1,GetPixel(j,col-i-1));
       PutPixel(j,col-i-1,b);
   }}
}

void Texture::Draw(int x,int y,int d)
{
   int i,j,k=0;
   register int b;
   unsigned char *vptr0 = (unsigned char*)VideoMem + 320*y + x;
   register unsigned char *vptr;
   int i0=0;
   int j0=0;
   int i1=col;
   int j1=row;
   if(d!=-1)
   {
      vptr0+=d;
      vptr0+=d*320;
      i0=j0=d;
      i1-=d;
      j1-=d;
   }
   for(j=j0;j<j1;j++)
   {  vptr = vptr0;
      for(i=i0;i<i1;i++)
      {  b=image[k++];
         if(b) *vptr=b;
         vptr++;
      }
      vptr0+=320;
   }
}

#endif

