// MY_VGA.HPP       Shabarshin A.A
// 320x200 256 colors.  12.03.1997
// for WATCOM           28.10.1997
// +DrawChar()          17.01.1998

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <graph.h>
#ifndef __MY_VGA_H
#define __MY_VGA_H
#ifndef NO_FONT
#include "font5x9.h"
#endif

#define MAX_CVGA 63
#ifndef BYTE
#define BYTE unsigned char
#endif
#define VADR0(x,y) ((x)+s320[(y)]+6)
#define VADR(x,y) ((x)+s320[(y)])

//#define dX   320
//#define dY   200
int dX = 320;
int dY = 200;

int fVGA=0;
long s320[200];
BYTE *VideoMem = NULL;

struct RGB
{  BYTE r,g,b;
   RGB(short r1,short g1,short b1){r=r1;g=g1;b=b1;};
   RGB(){r=MAX_CVGA;g=MAX_CVGA;b=MAX_CVGA;};
};

short _mode_;

void SetVideoMode(short mode)
{   _mode_=mode;
    _setvideomode(mode);
}

short InitGraph(short mode=0x13)
{   if(fVGA) return 0;
    fVGA=1;
    int j=0;
    for(long i=0;i<64000L;i+=320) s320[j++]=i;
    SetVideoMode(mode);
    long size = _imagesize(0,0,319,199);
    VideoMem = (BYTE*)(0xA000<<4);
    _setbkcolor(0L);
    return 1;
}

inline void CloseGraph(void)
{
    SetVideoMode(-1);
}

inline void PutScreenPixel(int x,int y,int c)
{
    VideoMem[VADR(x,y)]=c;
}

inline short GetScreenPixel(short x,short y)
{
    return VideoMem[VADR(x,y)];
}

short DrawChar(short x0,short y0,short ch,short cs,short cb)
{
    #ifdef NO_FONT
    printf("%c",ch);
    return 0;
    #else
    if(ch<32||ch>127) return 0;
    short i,j,k,r;
    unsigned char *vptr0 = VideoMem + VADR(x0,y0);
    unsigned char *vptr;
    for(j=0;j<9;j++)
    {  r = font5x9[ch-32][j];
       vptr = vptr0 + 320*j;
       *vptr++ = cb;
       for(i=0;i<5;i++)
       {  if(r&1) *vptr=cs;
          else *vptr=cb;
          r>>=1;
          vptr++;
       }
       *vptr++ = cb;
       *vptr++ = cb;
    }
    return 1;
    #endif
}

short DrawString(short x0,short y0,char *s,short cs,short cb)
{
    for(int i=0;i<strlen(s);i++)
    {
       int x = x0+i*8;
       if(x>=320) break;
       DrawChar(x,y0,s[i],cs,cb);
    }
    return 1;
}

short DrawBar(short x0,short y0,short dx,short dy,short c)
{
    short i,j;
    if(x0<0)
    {  dx=dx+x0;
       x0=0;
    }
    if(y0<0)
    {  dy=dy+y0;
       y0=0;
    }
    if((x0+dx)>=dX) dx=dX-x0;
    if((y0+dy)>=dY) dy=dY-y0;
    long ab=s320[y0]+x0,at;
    for(j=0;j<dy;j++,ab+=320){ at=ab;
    for(i=0;i<dx;i++,at++){
        VideoMem[at] = c;
    }}
    return c;
}

short HLine(short x0,short y0,short dx,short c)
{
    if(x0<0)
    {  dx=dx+x0;
       x0=0;
    }
    short i;
    long a = s320[y0] + x0;
    for(i=x0;i<x0+dx;i++,a++)
    {   if(i>=dX) break;
        VideoMem[a] = c;
    }
    return c;
}

short VLine(short x0,short y0,short dy,short c)
{
    if(y0<0)
    {  dy=dy+y0;
       y0=0;
    }
    short j;
    long a = s320[y0] + x0;
    for(j=y0;j<y0+dy;j++,a+=dX)
    {   if(j>=dY) break;
        VideoMem[a] = c;
    }
    return c;
}

struct Palette256
{
     BYTE d[256][3];
     Palette256(){InitGraph();};
     Palette256(char *p)
     {  InitGraph();
        LoadPalette(p);
        SetHardPalette();
     };
     short LoadPalette(char* s);
     short SavePalette(char* s);
    ~Palette256(){;};
     void SetPalette(short i,RGB col)
     {  d[i][0]=col.r;
        d[i][1]=col.g;
        d[i][2]=col.b;
     };
     void SetHardPalette(void)
     {  long pal[257],lr,lg,lb;
        for(short i=0;i<256;i++)
        {   lr = d[i][0];
            lg = d[i][1];
            lb = d[i][2];
            pal[i] = (lb<<16)|(lg<<8)|lr;
        }
        pal[16] = 0L;
        _remapallpalette(pal);
     };
     RGB GetPalette(short i)
     {  RGB col;
        col.r=d[i][0];
        col.g=d[i][1];
        col.b=d[i][2];
        return col;
     };
     void GetHardPalette(void)
     {

     };
};

short Palette256::LoadPalette(char* s)
{  short b,g,r,i;
   char ss[100];
   strcpy(ss,s);
   strcat(ss,".pal");
   FILE *f=fopen(ss,"rb");
   for(i=0;i<256;i++)
   {
       r=fgetc(f);
       g=fgetc(f);
       b=fgetc(f);
       SetPalette(i,RGB(r,g,b));
   }
   fclose(f);
   return 1;
}

short Palette256::SavePalette(char* s)
{  short i;
   RGB p;
   char ss[100];
   strcpy(ss,s);
   strcat(ss,".pal");
   FILE *f=fopen(ss,"wb");
   for(i=0;i<256;i++)
   {
       p=GetPalette(i);
       fputc(p.r,f);
       fputc(p.g,f);
       fputc(p.b,f);
   }
   fclose(f);
   return 1;
}

void TraceWait(void){};

#endif
