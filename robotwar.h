// ROBOTWAR.H - Shabarshin A.A.  10.11.1998

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#ifndef _ROBOTWAR_H
#define _ROBOTWAR_H

#define VARMAX  10000
#define MAXLINE 1000
#define STRLEN  32000
#define SENDBUF 100
#define DMBOX   10
#define NFREE   10

#define   MISSLEBEG  5
#define   MISSLEADD  10
#define   MISSLESPE  5
#define   ENERGYBEG  5
#define   ENERGWAIT  5
#define   ENERGYMAX  13

#define EYE 1
#define GUN 2

class Text;

class Robot
{
  short dx,dy,xr,yr;
  unsigned char **map;
  short cur,nvar,number,elec,magic;
  unsigned char *code;
  short *var;
  char name[32];
  char author[32];
  Robot **rar;
  short nrar;
  short f_gr;
  char *str;
  char *last;
  long color;
  unsigned char image[8][8];
  unsigned char equip[4];
  void Compile(Text *);
  short CompExpr(char *str,char *name1,char *name2,char *name3);
  void CompName(Text *v,char *pname,char *b,char *line,short n=0,short il=0);
  void StrUp(char *);
  int StartWith(char *str,char *sw,short f=0);
  char* SkipSpace(char* p);
  short Var(short f=0);
  short Adr(void);
  short Hex(char ch);
 public:
  short X,Y,D,N,K,R,T,E,M,S;
  short command,angle,halt;
  short fimage,pc,sp,send;
  short s_i,s_o;
  short s_k[SENDBUF];
  short s_n[SENDBUF];
  short s_t[SENDBUF];
  short s_x[SENDBUF];
  short s_y[SENDBUF];
  static void Error(int e,char *s=NULL,short i=0);
  Robot(short num);
  Robot(short num,char *);
 ~Robot();
  void SetMap(short x,short y,unsigned char **m){dx=x;dy=y;map=m;};
  void SetLoc(short x,short y){xr=x;yr=y;};
  void SetX(short xx){xr=xx;};
  void SetY(short yy){yr=yy;};
  short GetX(void){return xr;};
  short GetY(void){return yr;};
  short GetI(short i,short j){return image[i][j];};
  short GetR(void){return (color>>16)&0xFF;};
  short GetG(void){return (color>>8)&0xFF;};
  short GetB(void){return (color)&0xFF;};
  short GetE(short i){return equip[i];};
  short GetCode(short adr){return code[adr];};
  short GetNumber(void){return number;};
  char* GetName(void){return name;};
  char* GetAuthor(void){return author;};
  short GetLCod(void){return cur;};
  short GetLVar(void){return nvar;};
  short Damage(void){return --E;};
  void Init(void);
  char* Step(FILE *f=NULL);
  void Save(char *fname);
  void SetVar(short v){nvar=v;};
  void SetCode(short l,unsigned char *c){cur=l;code=c;};
  void SetName(char *n);
  void SetAuthor(char *n);
  void SetImage(int f,void *pv=NULL);
  void SetColor(int r,int g,int b){color=(((long)r)<<16)|(g<<8)|b;};
  void SetEquip(int e0,int e1,int e2,int e3)
       {equip[0]=e0;equip[1]=e1;equip[2]=e2;equip[3]=e3;};
  void SetLast(char *lsay){last=lsay;};
  short Prepro(Text *t);
  short Send(short k,short n,short t,short x,short y);
  void SetRar(Robot **ra,short nra,short fgr=0){rar=ra;nrar=nra;f_gr=fgr;};
};

extern double Rand;

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

short Random(short n);
unsigned char ** MakeMap(short n,short x=0,short y=0);

extern short _X_box[DMBOX];
extern short _Y_box[DMBOX];
extern short _N_box;

#endif
