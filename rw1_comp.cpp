/*
 Robot Warfare 1 Open Game
 =========================

 Copyright (c) 1998-2001 A.A.Shabarshin <me@shaos.net>

 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom
 the Software is furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

// RW1_COMP.CPP - Shabarshin A.A.  14.11.1998

// 1.99 - 18.12.1999

#include "robotwar.h"

int main(int argc,char **argv)
{
   printf("\n\nRobot Warfare 1 Open Compiler Ver 1.99.3 (c) 1998-2000, A.A.Shabarshin\n\n");
   if(argc<2)
   {
      printf("\nUsage:\n");
      printf("\n   RW1_COMP.EXE  FILE.RW1\n\n");
      exit(1);
   }
   char *po = argv[1];
   Robot r(0,po);
   r.Save(po);
   return 0;
}
